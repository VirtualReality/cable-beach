SetCompressor /FINAL zlib

Function .onInit

; prevent installer from being started twice
System::Call 'kernel32::CreateMutexA(i 0, i 0, t "CableBeachLauncherInstaller") i .r1 ?e'
Pop $R0
StrCmp $R0 0 +3
  MessageBox MB_OK|MB_ICONEXCLAMATION "The CableBeachLauncher installer is already running."
  Abort

SectionSetFlags SEC01 17 ; locks first section, ie forced to install

FunctionEnd

; HM NIS Edit Wizard helper defines
!define PRODUCT_NAME "CableBeachLauncher"
!define PRODUCT_VERSION "1.1.0"
!define PRODUCT_PUBLISHER "Intel Corporation"
!define PRODUCT_WEB_SITE "http://cablebeach.googlecode.com/"
!define PRODUCT_DIR_REGKEY "Software\Microsoft\Windows\CurrentVersion\App Paths\${PRODUCT_NAME}"
!define PRODUCT_UNINST_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}"
!define PRODUCT_UNINST_ROOT_KEY "HKLM"

VIProductVersion "1.1.0.0"
VIAddVersionKey "ProductName" "CableBeachLauncher"
VIAddVersionKey "Comments" ""
VIAddVersionKey "CompanyName" "Intel Corporation"
VIAddVersionKey "LegalTrademarks" "Modified BSD license"
VIAddVersionKey "LegalCopyright" "Copyright � Intel Corporation"
VIAddVersionKey "FileDescription" "CableBeachLauncher Installer"
VIAddVersionKey "FileVersion" "1.1.0"

; MUI 1.67 compatible ------
!include "MUI2.nsh"
BrandingText "CableBeachLauncher"
; MUI Settings
!define MUI_ABORTWARNING

!define MUI_ICON "${NSISDIR}\Contrib\Graphics\Icons\modern-install.ico"
!define MUI_UNICON "${NSISDIR}\Contrib\Graphics\Icons\modern-uninstall.ico"
!define MUI_LICENSEPAGE_BGCOLOR /grey
;BGGradient 001122 0058B0 FFFFFF
; Welcome page
!insertmacro MUI_PAGE_WELCOME
; License page
!insertmacro MUI_PAGE_LICENSE "LICENSE.txt"
; Components page
!insertmacro MUI_PAGE_COMPONENTS
; Directory page
!insertmacro MUI_PAGE_DIRECTORY
; Instfiles page
!insertmacro MUI_PAGE_INSTFILES
; Finish page
!define MUI_FINISHPAGE_LINK "ScienceSim"
!define MUI_FINISHPAGE_LINK_LOCATION "http://www.sciencesim.com/"
!insertmacro MUI_PAGE_FINISH

; Uninstaller pages
!insertmacro MUI_UNPAGE_INSTFILES

; Language files
!insertmacro MUI_LANGUAGE "English"

; MUI end ------
Name "${PRODUCT_NAME} ${PRODUCT_VERSION}"
OutFile "CableBeachLauncherInstaller.exe"
XPStyle on
InstallDir "$PROGRAMFILES\CableBeachLauncher"
InstallDirRegKey HKLM "${PRODUCT_DIR_REGKEY}" ""
ShowInstDetails show
ShowUnInstDetails show

; required base system!
Section "!CableBeachLauncher" SEC01
  SetOutPath "$INSTDIR"
  CreateDirectory "$SMPROGRAMS\CableBeachLauncher"
  SetOverwrite ifnewer
  File "*.exe"
  File "*.txt"
  CreateShortCut "$SMPROGRAMS\CableBeachLauncher\Cable Beach Launcher.lnk" "$INSTDIR\CableBeachLauncher.exe"
  CreateShortCut "$SMPROGRAMS\CableBeachLauncher\Uninstall.lnk" "$INSTDIR\uninst.exe"
SectionEnd

Section -Post
  WriteUninstaller "$INSTDIR\uninst.exe"
  WriteRegStr HKLM "${PRODUCT_DIR_REGKEY}" "" "$INSTDIR\CableBeachLauncher.exe"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayName" "$(^Name)"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "UninstallString" "$INSTDIR\uninst.exe"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayIcon" "$INSTDIR\CableBeachLauncher.exe"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayVersion" "${PRODUCT_VERSION}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "URLInfoAbout" "${PRODUCT_WEB_SITE}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "Publisher" "${PRODUCT_PUBLISHER}"
  
  ; URI handler
  WriteRegStr HKEY_CLASSES_ROOT "cablebeach" "" "URL:CableBeach"
  WriteRegStr HKEY_CLASSES_ROOT "cablebeach" "URL Protocol" ""
  WriteRegStr HKEY_CLASSES_ROOT "cablebeach\DefaultIcon" "" '"$INSTDIR\CableBeachLauncher.exe"'
  WriteRegExpandStr HKEY_CLASSES_ROOT "cablebeach\shell\open\command" "" '"$INSTDIR\CableBeachLauncher.exe" "%1"'
SectionEnd

; Section descriptions
!insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
  !insertmacro MUI_DESCRIPTION_TEXT ${SEC01} "Core components required for application usage"
!insertmacro MUI_FUNCTION_DESCRIPTION_END


Function un.onUninstSuccess
  HideWindow
  MessageBox MB_ICONINFORMATION|MB_OK "$(^Name) was successfully removed from your computer."
FunctionEnd

Function un.onInit
  MessageBox MB_ICONQUESTION|MB_YESNO|MB_DEFBUTTON2 "Are you sure you want to completely remove $(^Name) and all of its components?" IDYES +2
  Abort
FunctionEnd

Section Uninstall
  RMDir /r "$SMPROGRAMS\CableBeachLauncher"
  RMDir /r "$INSTDIR"

  DeleteRegKey ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}"
  DeleteRegKey HKLM "${PRODUCT_DIR_REGKEY}"
  SetAutoClose true
SectionEnd
