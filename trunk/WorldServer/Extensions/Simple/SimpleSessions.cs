﻿/* 
 * Copyright (c) Intel Corporation
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * -- Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * -- Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * -- Neither the name of the Intel Corporation nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE INTEL OR ITS
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

using System;
using System.Collections.Generic;
using ExtensionLoader;
using OpenMetaverse;

namespace WorldServer.Extensions
{
    public class SimpleSessions : IExtension<WorldServer>, ISessionProvider
    {
        WorldServer server;
        ExpiringCache<UUID, Avatar> sessionIDToAvatar = new ExpiringCache<UUID, Avatar>();
        Dictionary<Uri, UUID> identityTosessionID = new Dictionary<Uri, UUID>();
        object syncRoot = new object();

        public SimpleSessions()
        {
        }

        public bool Start(WorldServer server)
        {
            this.server = server;
            return true;
        }

        public void Stop()
        {
        }

        public UUID CreateSession(Avatar avatar)
        {
            lock (syncRoot)
            {
                RemoveSession(avatar.Identity);

                UUID sessionID = UUID.Random();
                sessionIDToAvatar.AddOrUpdate(sessionID, avatar, TimeSpan.FromMinutes(30));
                identityTosessionID[avatar.Identity] = sessionID;
                return sessionID;
            }
        }

        public bool RemoveSession(UUID sessionID)
        {
            lock (syncRoot)
            {
                Avatar avatar;
                if (sessionIDToAvatar.TryGetValue(sessionID, out avatar))
                {
                    identityTosessionID.Remove(avatar.Identity);
                    return sessionIDToAvatar.Remove(sessionID);
                }
                else
                {
                    return false;
                }
            }
        }

        public bool RemoveSession(Uri identity)
        {
            lock (syncRoot)
            {
                UUID sessionID;
                if (identityTosessionID.TryGetValue(identity, out sessionID))
                {
                    sessionIDToAvatar.Remove(sessionID);
                    return identityTosessionID.Remove(identity);
                }
                else
                {
                    return false;
                }
            }
        }

        public bool TryGetAvatar(UUID sessionID, out Avatar avatar)
        {
            lock (syncRoot)
                return sessionIDToAvatar.TryGetValue(sessionID, out avatar);
        }

        public bool TryGetSession(Uri identity, out UUID sessionID)
        {
            lock (syncRoot)
                return identityTosessionID.TryGetValue(identity, out sessionID);
        }

        public int CurrentSessionCount()
        {
            return sessionIDToAvatar.Count;
        }
    }
}
