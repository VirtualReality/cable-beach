﻿/* 
 * Copyright (c) Intel Corporation
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * -- Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * -- Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * -- Neither the name of the Intel Corporation nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE INTEL OR ITS
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

using System;
using ExtensionLoader.Config;
using OpenSimDbLinq;

namespace WorldServer.Extensions
{
    public static class OpenSimUtils
    {
        public static OpenSimDatabase LoadDatabaseFromConfig(IConfig config)
        {
            string dbServer, dbUser, dbPass, dbName, dbFilename;
            string dbEngine = config.GetString("DatabaseEngine");

            if (String.IsNullOrEmpty(dbEngine))
                return null;

            switch (dbEngine.Trim().ToLower())
            {
                case "mysql":
                    dbServer = config.GetString("DatabaseServer");
                    dbUser = config.GetString("DatabaseUser");
                    dbPass = config.GetString("DatabasePass");
                    dbName = config.GetString("DatabaseName");
                    return OpenSimDatabase.CreateFromMySql(dbServer, dbUser, dbPass, dbName);
                case "mssql":
                    dbServer = config.GetString("DatabaseServer");
                    dbName = config.GetString("DatabaseName");
                    return OpenSimDatabase.CreateFromMsSql(dbServer, dbName);
                case "postgresql":
                    dbServer = config.GetString("DatabaseServer");
                    dbUser = config.GetString("DatabaseUser");
                    dbPass = config.GetString("DatabasePass");
                    dbName = config.GetString("DatabaseName");
                    return OpenSimDatabase.CreateFromPostgres(dbServer, dbUser, dbPass, dbName);
                case "sqlite":
                    dbFilename = config.GetString("DatabaseFilename");
                    return OpenSimDatabase.CreateFromSqlite(dbFilename);
                default:
                    Logger.Error("[LindenLogin] Unrecognized database engine \"" + dbEngine + "\"");
                    return null;
            }
        }
    }
}
