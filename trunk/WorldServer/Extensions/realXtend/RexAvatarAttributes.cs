﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WorldServer.Extensions
{
    public static class RexAvatarAttributes
    {
        public static readonly Uri AUTH_URI = new Uri("http://www.realxtend.org/attributes/authURI");
        public static readonly Uri ACCOUNT = new Uri("http://www.realxtend.org/attributes/account");
        public static readonly Uri REALNAME = new Uri("http://www.realxtend.org/attributes/realname");
        public static readonly Uri SESSIONHASH = new Uri("http://www.realxtend.org/attributes/sessionhash");
        public static readonly Uri AVATAR_STORAGE_URL = new Uri("http://www.realxtend.org/attributes/avatarStorageUri");
        public static readonly Uri SKYPE_URL = new Uri("http://www.realxtend.org/attributes/skypeUrl");
        public static readonly Uri GRID_URL = new Uri("http://www.realxtend.org/attributes/gridUrl");
    }
}
