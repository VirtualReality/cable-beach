﻿/* 
 * Copyright (c) Intel Corporation
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * -- Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * -- Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * -- Neither the name of the Intel Corporation nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE INTEL OR ITS
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

using System;
using System.Collections.Generic;
using System.Net;
using HttpServer;
using Nwc.XmlRpc;
using OpenMetaverse;
using CableBeachMessages;
using OpenMetaverse.StructuredData;

namespace WorldServer
{
    /// <summary>
    /// Response from a call to a backend provider
    /// </summary>
    public enum BackendResponse
    {
        /// <summary>The call succeeded</summary>
        Success,
        /// <summary>The resource requested was not found</summary>
        NotFound,
        /// <summary>A server failure prevented the call from
        /// completing</summary>
        Failure
    }

    /// <summary>
    /// Callback for an XML-RPC request
    /// </summary>
    /// <param name="request">XML-RPC request data</param>
    /// <param name="httpRequest">Reference to the underlying HTTP request</param>
    /// <returns>XML-RPC response data</returns>
    public delegate XmlRpcResponse XmlRpcMethod(XmlRpcRequest request, IHttpRequest httpRequest);

    public interface ISessionProvider
    {
        UUID CreateSession(Avatar avatar);
        bool RemoveSession(UUID sessionID);
        bool RemoveSession(Uri identity);
        bool TryGetAvatar(UUID sessionID, out Avatar avatar);
        bool TryGetSession(Uri identity, out UUID authToken);

        int CurrentSessionCount();
    }

    public interface IMapProvider
    {
        BackendResponse TryFetchRegion(UUID id, out RegionInfo region);
        BackendResponse TryFetchRegion(uint x, uint y, out RegionInfo region);
        BackendResponse TryFetchRegion(string name, out RegionInfo region);
        BackendResponse TryFetchRegionNearest(uint x, uint y, out RegionInfo region);
        BackendResponse TryFetchDefaultRegion(out RegionInfo region);

        BackendResponse RegionSearch(string query, out List<RegionInfo> results);
        int RegionCount();
        int OnlineRegionCount();

        int ForEach(Action<RegionInfo> action, int start, int count);
    }

    public interface IAttributeProvider
    {
        Uri UUIDToIdentity(UUID avatarID);
        Dictionary<Uri, OSD> GetAttributes(Uri identity);
        void UpdateAttributes(Uri identity, Dictionary<Uri, OSD> attributes);
    }

    public interface IServiceProvider
    {
        void GetServices(Uri identity, bool fetchTrustedCapabilities, ref ServiceCollection services);
    }

    public interface IXmlRpcProvider
    {
        void AddXmlRpcHandler(string path, string methodName, XmlRpcMethod method);
    }
}
