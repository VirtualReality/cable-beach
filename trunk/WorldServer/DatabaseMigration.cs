﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OpenSim.Data;
using OpenSim.Data.MySQL;
using OpenSim.Data.MSSQL;

namespace WorldServer
{
    public class DatabaseMigration
    {
        private string connectionString;
        private IUserDataPlugin userData;

        public DatabaseMigration(string databaseType, string connString)
        {
            connectionString = connString;
            if (databaseType == "mssql")
                userData = new MSSQLUserData();
            else if (databaseType == "mysql")
                userData = new MySQLUserData();
            Logger.InfoFormat("[DatabaseMigration] Connection string set to {0}", connectionString);
        }

        public bool MigrateUserTables()
        {
            try
            {
                userData.Initialise(connectionString);
                Logger.Info("[DatabaseMigration] User migration succesfull");
                return true;
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("[DatabaseMigration] Could not migrate user database with connection string {0}", connectionString);
                return false;
            }
            
        }
    }
}
