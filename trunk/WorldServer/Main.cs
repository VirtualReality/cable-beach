﻿/* 
 * Copyright (c) Intel Corporation
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * -- Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * -- Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * -- Neither the name of the Intel Corporation nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE INTEL OR ITS
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

using System;
using System.ServiceProcess;

namespace WorldServer
{
    class MainEntry
    {
        static void Main(string[] args)
        {
#if SERVICE
            ServiceBase[] servicesToRun = new ServiceBase[] { new WorldServer() }; 
            ServiceBase.Run(servicesToRun);
#else
            WorldServer server = new WorldServer();
            if (server.Start())
            {
                Console.WriteLine("");
                Console.WriteLine("CableBeach WorldServer is now running. Use 'help' to get console commands.");
                Console.WriteLine("");
                Console.CancelKeyPress +=
                    delegate(object sender, ConsoleCancelEventArgs e)
                    {
                        Console.WriteLine("CableBeach WorldServer is shutting down...");
                        server.Shutdown();
                        Environment.Exit(0);
                    };


                string command;

                while (true)
                {
                    Console.Write("CB WorldServer: ");
                    command = Console.ReadLine();
                    switch (command)
                    {
                        case "help":
                            Console.WriteLine("    help        - Shows this help information");
                            Console.WriteLine("    quit        - Shuts server down");
                            Console.WriteLine("    exit        - Shuts server down");
                            Console.WriteLine("    create user - Creates new OpenSim user for this server");
                            break;

                        case "quit":
                        case "exit":
                            server.Shutdown();
                            Environment.Exit(0);
                            break;

                        case "create user":
                            server.UserManager.CreateUser();
                            break;


                    }
                }
            }
#endif
        }
    }
}
