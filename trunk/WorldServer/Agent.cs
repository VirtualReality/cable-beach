using System;
using System.Collections.Generic;
using System.Text;
using OpenMetaverse;

namespace WorldServer
{
    public class Agent
    {
        public UUID UUID;
        public UUID SessionID;
        public UUID SecureSessionID;
        public string AgentIP = String.Empty;
        public uint AgentPort;
        public bool AgentOnline;
        public int LoginTime;
        public int LogoutTime;
        public UUID RegionID;
        public ulong RegionHandle;
        public Vector3 CurrentPos;
        public Vector3 CurrentLookAt = Vector3.Zero;
        public UUID OriginRegionID;

    }
}
