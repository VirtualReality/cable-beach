﻿/* 
 * Copyright (c) Intel Corporation
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * -- Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * -- Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * -- Neither the name of the Intel Corporation nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE INTEL OR ITS
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Reflection;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.ServiceProcess;
using ExtensionLoader;
using ExtensionLoader.Config;
using HttpServer;
using HttpServer.Templates;
using Mono.Security.X509;
using OpenMetaverse;
using OpenMetaverse.Http;
using CableBeachMessages;

using HttpListener = HttpServer.HttpListener;

namespace WorldServer
{
    public class WorldServer : ServiceBase
    {
        public const string DATA_DIR = "CableBeachData/";
        public const string WEB_CONTENT_DIR = DATA_DIR + "webcontent/";
        public const string CONFIG_FILE = DATA_DIR + "WorldServer.ini";
        public const string DEFAULT_SERVICE_CERT = DATA_DIR + "TrustedService.p12";

        public Uri HttpUri;
        public HttpListener HttpServer;
        public SmartyEngine HttpTemplates = new SmartyEngine();
        public X509Certificate2 HttpCertificate;
        public RSA HttpSigningKey;
        public CapsServer CapsServer;
        public IniConfigSource ConfigFile;
        public List<string> ExtensionList;
        public UserManager UserManager;
        private DatabaseMigration DatabaseMigration; 

        public ISessionProvider SessionProvider;
        public IMapProvider MapProvider;
        public IAttributeProvider AttributeProvider;
        public IServiceProvider ServiceProvider;
        public IXmlRpcProvider XmlRpcProvider;

        ExtensionLoader<WorldServer> extensions = new ExtensionLoader<WorldServer>();
        HttpServer.Handlers.FileHandler fileHandler;

        public WorldServer()
        {
            this.ServiceName = "CableBeachWorldServer";
        }

        public bool Start()
        {
            IPHostEntry entry;
            IPAddress address;
            IConfig httpConfig;
            IConfig databaseConfig;

            #region Config Parsing

            try
            {
                // Load the extension list (and ordering) from our config file
                ConfigFile = new IniConfigSource(CONFIG_FILE);
                httpConfig = ConfigFile.Configs["Http"];
                IConfig extensionConfig = ConfigFile.Configs["Extensions"];
                ExtensionList = new List<string>(extensionConfig.GetKeys());
                // Migrate user database
                databaseConfig = ConfigFile.Configs["OpenSimUserDatabase"];
                DatabaseMigration = new DatabaseMigration(databaseConfig.Get("DatabaseType"), databaseConfig.Get("ConnectionString"));
                DatabaseMigration.MigrateUserTables();
                UserManager = new UserManager(databaseConfig);
                
            }
            catch (Exception)
            {
                Logger.Error("[WorldServer] Failed to parse " + CONFIG_FILE);
                return false;
            }

            int port = httpConfig.GetInt("ListenPort", 0);
            string hostname = httpConfig.GetString("Hostname", null);
            string sslCertFile = httpConfig.GetString("SSLCertFile", null);

            if (port <= 0)
            {
                Logger.Error("[WorldServer] Invalid ListenPort value in the config file");
                return false;
            }

            if (String.IsNullOrEmpty(hostname))
            {
                hostname = Dns.GetHostName();
                entry = Dns.GetHostEntry(hostname);
                address = IPAddress.Any;
            }
            else
            {
                entry = Dns.GetHostEntry(hostname);
                if (entry != null && entry.AddressList.Length > 0)
                {
                    address = entry.AddressList[0];
                }
                else
                {
                    Logger.Warn("[WorldServer] Could not resolve an IP address from hostname " + hostname + ", binding to all interfaces");
                    address = IPAddress.Any;
                }
            }

            #endregion Config Parsing

            #region Certificate Loading/Creation

            // SSL root certificate and signing key loading/creation
            if (!String.IsNullOrEmpty(sslCertFile))
            {
                try
                {
                    byte[] p12Bytes = File.ReadAllBytes(DATA_DIR + sslCertFile);
                    PKCS12 p12 = new PKCS12(p12Bytes);
                    HttpCertificate = new X509Certificate2(p12Bytes);
                    HttpSigningKey = (RSA)p12.Keys[0];
                }
                catch (Exception ex)
                {
                    if (!File.Exists(DATA_DIR + sslCertFile))
                    {
                        try
                        {
                            Logger.Warn("[WorldServer] PKCS#12 file \"" + DATA_DIR + sslCertFile +
                                "\" does not exist, creating a new root certificate and signing key");

                            byte[] rootCert;
                            Trusted.CreateRootCert(hostname, out rootCert);
                            File.WriteAllBytes(DATA_DIR + sslCertFile, rootCert);

                            PKCS12 p12 = new PKCS12(File.ReadAllBytes(DATA_DIR + sslCertFile));
                            HttpCertificate = new X509Certificate2(p12.Certificates[0].RawData);
                            HttpSigningKey = (RSA)p12.Keys[0];

                            Logger.Info("[WorldServer] New root certificate and signing key saved into " + DATA_DIR + sslCertFile);

                            if (!File.Exists(DEFAULT_SERVICE_CERT))
                            {
                                Logger.Info("[WorldServer] Creating default service certificate \"" + DEFAULT_SERVICE_CERT + "\"");

                                try
                                {
                                    Mono.Security.X509.X509Certificate monoRootCert = p12.Certificates[0];
                                    byte[] defaultServiceCert = Trusted.CreateSignedCert("default", HttpSigningKey, monoRootCert);
                                    File.WriteAllBytes(DEFAULT_SERVICE_CERT, defaultServiceCert);

                                    Logger.Info("[WorldServer] New default service certificate saved into " + DEFAULT_SERVICE_CERT);
                                }
                                catch (Exception ex2)
                                {
                                    Logger.Error("[WorldServer] Failed to create default service certificate: " + ex2.Message);
                                }
                            }
                        }
                        catch (Exception ex2)
                        {
                            Logger.Error("[WorldServer] Failed to create root certificate and signing key: " + ex2.Message);
                        }
                    }
                    else
                    {
                        Logger.Error("[WorldServer] Failed to load SSL certificate file \"" + DATA_DIR + sslCertFile + "\": " + ex.Message);
                    }

                    Logger.Info("[WorldServer] New certificates were generated. Restart the other Cable Beach services before restarting WorldServer");
                    return false;
                }
            }

            #endregion Certificate Loading/Creation

            #region HTTP Server

            if (httpConfig.GetBoolean("SSLServer", false))
            {
                // HTTPS mode
                if (HttpCertificate == null)
                {
                    Logger.Error("[WorldServer] SSLServer is set to true but no valid SSL certificate could be loaded");
                    return false;
                }
                else
                {
                    HttpServer = HttpListener.Create(log4netLogWriter.Instance, address, port, HttpCertificate);
                    HttpUri = new Uri("https://" + hostname + (port != 80 ? (":" + port) : String.Empty));
                }
            }
            else
            {
                // HTTP mode
                HttpServer = HttpListener.Create(log4netLogWriter.Instance, address, port);
                HttpUri = new Uri("http://" + hostname + (port != 80 ? (":" + port) : String.Empty));
            }

            HttpServer.Set404Handler(NotFoundHandler);
            HttpServer.Start(10);

            // File serving for web templates
            fileHandler = new HttpServer.Handlers.FileHandler(HttpServer, "/content/", WEB_CONTENT_DIR, true);

            // Capabilities handling
            CapsServer = new CapsServer(HttpServer, @"^/caps/");
            CapsServer.Start();

            Logger.Info("[WorldServer] WorldServer is listening at " + HttpUri.ToString());

            #endregion HTTP Server

            #region Server Extensions

            try
            {
                // Create a list of references for .cs extensions that are compiled at runtime
                List<string> references = new List<string>();
                references.Add("OpenMetaverseTypes.dll");
                references.Add("OpenMetaverse.dll");
                references.Add("OpenMetaverse.StructuredData.dll");
                references.Add("OpenMetaverse.Http.dll");
                references.Add("ExtensionLoader.dll");
                references.Add("WorldServer.exe");

                // Load extensions from the current executing assembly, WorldServer.*.dll assemblies on disk, and
                // WorldServer.*.cs source files on disk.
                extensions.LoadAllExtensions(Assembly.GetExecutingAssembly(),
                    AppDomain.CurrentDomain.BaseDirectory, ExtensionList, references,
                    "WorldServer.*.dll", "WorldServer.*.cs");

                // Automatically assign extensions that implement interfaces to the list of interface
                // variables in "assignables"
                extensions.AssignExtensions(this, extensions.GetInterfaces(this));
            }
            catch (ExtensionException ex)
            {
                Logger.Error("[WorldServer] Extension loading failed, shutting down: " + ex.Message);
                Shutdown();
                return false;
            }

            // Start all of the extensions
            foreach (IExtension<WorldServer> extension in extensions.Extensions)
            {
                Logger.Info("[WorldServer] Starting extension " + extension.GetType().Name);
                if (!extension.Start(this))
                {
                    Logger.Error("[WorldServer] Extension initialization failed, shutting down");
                    Shutdown();
                    return false;
                }
            }

            #endregion Server Extensions

            return true;
        }

        public void Shutdown()
        {
            foreach (IExtension<WorldServer> extension in extensions.Extensions)
            {
                Logger.Debug("[WorldServer] Stopping extension " + extension.GetType().Name);
                try { extension.Stop(); }
                catch (Exception ex)
                { Logger.ErrorFormat("[WorldServer] Failure shutting down extension {0}: {1}", extension.GetType().Name, ex.Message); }
            }

            if (HttpServer != null)
                HttpServer.Stop();

#if !DEBUG
            Stop();
#endif
        }

        void NotFoundHandler(IHttpClientContext context, IHttpRequest request, IHttpResponse response)
        {
            Logger.Warn("[WorldServer] Returning 404 for " + request.Method + " " + request.UriPath);
            response.Status = HttpStatusCode.NotFound;
            response.AddToBody("<html><head><title>Not Found</title></head><body><h3>The requested document was not found</h3></body></html>");
        }

        #region ServiceBase Overrides

        protected override void OnStart(string[] args)
        {
            Start();
        }
        protected override void OnStop()
        {
            Shutdown();
        }

        #endregion
    }
}
