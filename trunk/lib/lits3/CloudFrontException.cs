﻿using System;
using System.Net;
using System.Xml;

namespace LitS3
{
    /// <summary>
    /// The exception that is thrown when the Amazon server returns a specially formatted error object
    /// that we can parse.
    /// </summary>
    public sealed class CloudFrontException : Exception
    {
        /// <summary>
        /// Gets the error code returned by CloudFront.
        /// </summary>
        public CloudFrontErrorCode ErrorCode { get; private set; }

        public CloudFrontException(CloudFrontErrorCode errorCode, string bucketName, string message, WebException innerException)
            : base(message, innerException)
        {
            this.ErrorCode = errorCode;
        }

        internal static CloudFrontException FromWebException(WebException exception)
        {
            HttpWebResponse response = (HttpWebResponse)exception.Response;

            var reader = new XmlTextReader(response.GetResponseStream())
            {
                WhitespaceHandling = WhitespaceHandling.Significant,
                Namespaces = false
            };

            reader.MoveToContent();
            reader.ReadStartElement("Error");

            CloudFrontErrorCode errorCode = CloudFrontErrorCode.Unknown;
            string message = null;
            string bucketName = null;

            while (reader.Name != "Error")
            {
                switch (reader.Name)
                {
                    case "Code":
                        errorCode = ParseCode(reader.ReadElementContentAsString());
                        break;
                    case "Message":
                        message = reader.ReadElementContentAsString();
                        break;
                    default:
                        reader.Skip();
                        break;
                }
            }

            return new CloudFrontException(errorCode, bucketName, message, exception);
        }

        static CloudFrontErrorCode ParseCode(string code)
        {
            if (Enum.IsDefined(typeof(CloudFrontErrorCode), code))
                return (CloudFrontErrorCode)Enum.Parse(typeof(CloudFrontErrorCode), code);
            else
                return CloudFrontErrorCode.Unknown;
        }
    }
}
