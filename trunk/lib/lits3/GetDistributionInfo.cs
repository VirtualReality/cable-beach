﻿using System;
using System.IO;
using System.Net;
using System.Text;

namespace LitS3
{
    /// <summary>
    /// Creates a new CloudFront distribution.
    /// </summary>
    public class GetDistributionInfoRequest : CloudFrontRequest<GetDistributionInfoResponse>
    {
        public GetDistributionInfoRequest(CloudFrontService service, string distributionID)
            : base(service, "GET", distributionID, null)
        {
        }

        /*public override CreateDistributionResponse GetResponse()
        {
            AuthorizeIfNecessary();

            return base.GetResponse();
        }

        public override IAsyncResult BeginGetResponse(AsyncCallback callback, object state)
        {
            throw new InvalidOperationException("BeginGetResponse() is not supported for this class yet.");
        }*/
    }

    /// <summary>
    /// Represents a CloudFront response for a created bucket.
    /// </summary>
    public class GetDistributionInfoResponse : CloudFrontResponse
    {
        public Distribution Distribution { get; private set; }

        protected override void ProcessResponse()
        {
            Distribution = new Distribution(Reader);
        }
    }
}
