﻿
namespace LitS3
{
    /// <summary>
    /// Represents the possible error codes that can be returned by CloudFront.
    /// </summary>
    /// <remarks>
    /// The list of codes was parsed from here:
    /// http://docs.amazonwebservices.com/AmazonCloudFront/2008-06-30/DeveloperGuide/Errors.html
    /// </remarks>
    public enum CloudFrontErrorCode
    {
        Unknown,
        AccessDenied,
        InappropriateXML,
        InternalError,
        InvalidAction,
        InvalidArgument,
        InvalidClientTokenId,
        InvalidHTTPAuthHeader,
        InvalidHTTPRequest,
        InvalidURI,
        MalformedXML,
        MissingClientTokenId,
        MissingDateHeader,
        NoSuchVersion,
        NotImplemented,
        OptInRequired,
        RequestExpired,
        SignatureDoesNotMatch
    }
}
