﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Net;

namespace LitS3
{
    /// <summary>
    /// Describes how to connect to a particular CloudFront server.
    /// </summary>
    public class CloudFrontService
    {
        string secretAccessKey;
        CloudFrontAuthorizer authorizer;

        internal CloudFrontAuthorizer Authorizer { get { return authorizer; } }

        /// <summary>
        /// Gets or sets the hostname of the CloudFront server, usually "cloudfront.amazonaws.com" unless you
        /// are using a 3rd party S3 implementation.
        /// </summary>
        public string Host { get; set; }

        /// <summary>
        /// Gets or sets a custom port to use to connect to the S3 server. The default is zero, which
        /// will let this class auto-select the port based on the UseSsl property.
        /// </summary>
        public int CustomPort { get; set; }

        /// <summary>
        /// Gets or sets the Amazon Access Key ID to use for authentication purposes.
        /// </summary>
        public string AccessKeyID { get; set; }

        /// <summary>
        /// Gets or sets the Amazon Secret Access Key to use for authentication purposes.
        /// </summary>
        public string SecretAccessKey
        {
            get { return secretAccessKey; }
            set
            {
                secretAccessKey = value;
                authorizer = new CloudFrontAuthorizer(this);
            }
        }

        /// <summary>
        /// Gets or sets the CloudFront base URL. This is currently /2008-06-30/ until
        /// the API changes.
        /// </summary>
        public string BasePath { get; set; }

        /// <summary>
        /// Creates a new S3Service with the default values.
        /// </summary>
        public CloudFrontService()
        {
            this.Host = "cloudfront.amazonaws.com";
            this.BasePath = "/2008-06-30/distribution";
        }

        //#region Basic Operations

        /// <summary>
        /// Lists all distributions owned by you.
        /// </summary>
        public List<Distribution> GetAllDistributions()
        {
            using (GetAllDistributionsResponse response = new GetAllDistributionsRequest(this).GetResponse())
                return new List<Distribution>(response.Distributions);
        }

        public Distribution CreateDistribution(DistributionConfig config)
        {
            CreateDistributionRequest request = new CreateDistributionRequest(this, config);
            using (CreateDistributionResponse response = request.GetResponse())
            {
                return response.Distribution;
            }
        }

        public Distribution GetDistributionInfo(string distributionID)
        {
            GetDistributionInfoRequest request = new GetDistributionInfoRequest(this, distributionID);
            using (GetDistributionInfoResponse response = request.GetResponse())
            {
                return response.Distribution;
            }
        }
    }
}
