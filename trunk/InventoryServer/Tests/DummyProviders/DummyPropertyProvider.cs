﻿using System;
using System.Collections.Generic;
using System.Text;
using WebDAVSharp;

namespace InventoryServer.Tests
{
    internal class DummyPropertyProvider : IPropertyProvider
    {
        private Dictionary<string, IWebDAVResource> properties = new Dictionary<string, IWebDAVResource>();

        #region IPropertyProvider Members

        public bool Save(IWebDAVResource resource)
        {
            properties[resource.Path] = resource;
            Console.WriteLine("Set property " + resource.Path);
            return true;
        }

        public WebDAVSharp.IWebDAVResource Load(string path)
        {
            if (!properties.ContainsKey(path))
            {
                if (path.EndsWith("/"))
                {
                    return new WebDAVSharp.WebDAVFolder(path, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue, false);
                }
                else
                {
                    return new WebDAVSharp.WebDAVFile(path, "", 0, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue, false, false);
                }
            }
            else
            {
                return properties[path];
            }
        }

        #endregion
    }
}
