﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryServer.Tests
{
    internal class DummyFileSystemProvider : IFilesystemProvider
    {
        public InventoryCollection m_inventory = null;

        private void CreateUsersInventory(System.Uri owner, OpenMetaverse.UUID agentID)
        {
            m_inventory = new InventoryCollection();
            m_inventory.Folders = new Dictionary<OpenMetaverse.UUID, InventoryFolder>();
            m_inventory.Items = new Dictionary<OpenMetaverse.UUID, InventoryItem>();
            m_inventory.RootFolderID = new OpenMetaverse.UUID("00000000-0000-0000-0000-000000000004");
            m_inventory.UserID = agentID;
            InventoryFolder folder = new InventoryFolder("root", agentID, OpenMetaverse.UUID.Zero, 0);
            folder.ID = m_inventory.RootFolderID;
            m_inventory.Folders.Add(m_inventory.RootFolderID, folder);
        }

        private bool FindFolderFromCollection(InventoryFolder inventoryFolder, OpenMetaverse.UUID folderID, out InventoryFolder folder)
        {
            folder = null;
            foreach (var item in inventoryFolder.Children.Values)
            {
                if (item is InventoryFolder)
                {
                    if (item.ID == folderID)
                    {
                        folder = (InventoryFolder)item;
                        return true;
                    }
                    else
                    {
                        bool found = FindFolderFromCollection((InventoryFolder)item, folderID, out folder);
                        if (found)
                            return true;
                    }
                }
            }
            return false;
        }

        #region IFilesystemProvider Members

        public BackendResponse TryFetchItem(System.Uri owner, OpenMetaverse.UUID agentID, OpenMetaverse.UUID itemID, out InventoryItem item)
        {
            throw new System.NotImplementedException();
        }

        public BackendResponse TryFetchFolder(System.Uri owner, OpenMetaverse.UUID agentID, OpenMetaverse.UUID folderID, out InventoryFolder folder)
        {
            if (m_inventory == null) CreateUsersInventory(owner, agentID);
            folder = null;
            if (folderID == m_inventory.RootFolderID)
            {
                folder = m_inventory.Folders[m_inventory.RootFolderID];
                return BackendResponse.Success;
            }
            if (FindFolderFromCollection(m_inventory.Folders[m_inventory.RootFolderID], folderID, out folder))
            {
                return BackendResponse.Success;
            }
            return BackendResponse.Failure;
        }

        public BackendResponse TryFetchRootFolder(System.Uri owner, OpenMetaverse.UUID agentID, out InventoryFolder rootfolder)
        {
            if (m_inventory == null) CreateUsersInventory(owner, agentID);
            rootfolder = m_inventory.Folders[m_inventory.RootFolderID];
            foreach (InventoryFolder folder in m_inventory.Folders.Values)
            {
                if (folder.ParentID == rootfolder.ID)
                    rootfolder.Children.Add(folder.ID, folder);
            }
            return BackendResponse.Success;
        }

        public BackendResponse TryFetchFolderContents(System.Uri owner, OpenMetaverse.UUID agentID, OpenMetaverse.UUID folderID, out InventoryCollection contents)
        {
            throw new System.NotImplementedException();
        }

        public BackendResponse TryFetchFolderList(System.Uri owner, OpenMetaverse.UUID agentID, out System.Collections.Generic.List<InventoryFolder> folders)
        {
            throw new System.NotImplementedException();
        }

        public BackendResponse TryFetchFilesystem(System.Uri owner, OpenMetaverse.UUID agentID, out InventoryCollection inventory)
        {
            if (m_inventory == null)
            {
                CreateUsersInventory(owner, agentID);
            }
            inventory = m_inventory;
            return BackendResponse.Success;
        }

        public BackendResponse TryCreateItem(System.Uri owner, OpenMetaverse.UUID agentID, InventoryItem item)
        {
            InventoryFolder parent = null;
            TryFetchFolder(owner, agentID, item.ParentID, out parent);
            if (parent != null)
            {
                parent.Children.Add(item.ID, item);
                return BackendResponse.Success;
            }
            Console.WriteLine("Could not get folder for id");
            return BackendResponse.Failure;
        }

        public BackendResponse TryCreateFolder(System.Uri owner, OpenMetaverse.UUID agentID, InventoryFolder folder)
        {
            if (m_inventory == null) CreateUsersInventory(owner, agentID);
            m_inventory.Folders.Add(folder.ID, folder);
            return BackendResponse.Success;
        }

        public BackendResponse TryCreateFilesystem(System.Uri owner, OpenMetaverse.UUID agentID, InventoryFolder rootFolder)
        {
            throw new System.NotImplementedException();
        }

        public BackendResponse TryDeleteItem(System.Uri owner, OpenMetaverse.UUID agentID, OpenMetaverse.UUID itemID)
        {
            throw new System.NotImplementedException();
        }

        public BackendResponse TryDeleteFolder(System.Uri owner, OpenMetaverse.UUID agentID, OpenMetaverse.UUID folderID)
        {
            m_inventory.Folders.Remove(folderID);
            InventoryFolder deleteFolder = null;
            if(BackendResponse.Success == TryFetchFolder(owner, agentID, folderID, out deleteFolder))
            {
                InventoryFolder parentFolder = null;
                TryFetchFolder(owner, agentID, deleteFolder.ParentID, out parentFolder);
                parentFolder.Children.Remove(folderID);
            }
            return BackendResponse.Success;
        }

        public BackendResponse TryPurgeFolder(System.Uri owner, OpenMetaverse.UUID agentID, OpenMetaverse.UUID folderID)
        {
            throw new System.NotImplementedException();
        }

        public BackendResponse TryFetchActiveGestures(System.Uri owner, OpenMetaverse.UUID agentID, out System.Collections.Generic.List<InventoryItem> gestures)
        {
            throw new System.NotImplementedException();
        }

        public OpenMetaverse.UUID GetDefaultAsset(System.Uri owner, OpenMetaverse.UUID agentID, string contentType)
        {
            throw new System.NotImplementedException();
        }

        public OpenMetaverse.UUID GetDefaultParent(System.Uri owner, OpenMetaverse.UUID agentID, string contentType)
        {
            return new OpenMetaverse.UUID("00000000-0000-0000-0000-000000000004");
        }

        public OpenMetaverse.UUID IdentityToUUID(System.Uri owner)
        {
            return new OpenMetaverse.UUID("043d6d9f-d740-4ab2-98ab-11c908ff5c44");
        }

        #endregion
    }
}
