﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryServer.Tests
{
    internal class DummyAssetProvider : IAssetProvider
    {
        Dictionary<OpenMetaverse.UUID, KeyValuePair<CableBeachMessages.MetadataBlock, byte[]>> assets = new Dictionary<OpenMetaverse.UUID, KeyValuePair<CableBeachMessages.MetadataBlock, byte[]>>();

        #region IAssetProvider Members

        public int ForEach(Action<CableBeachMessages.MetadataBlock> action, int start, int count)
        {
            throw new NotImplementedException();
        }

        public BackendResponse TryCreateAsset(CableBeachMessages.MetadataBlock metadata, byte[] assetData, out OpenMetaverse.UUID assetID)
        {
            throw new NotImplementedException();
        }

        public BackendResponse TryCreateAsset(CableBeachMessages.MetadataBlock metadata, byte[] assetData)
        {
            if (assets.ContainsKey(metadata.ID))
            {
                return BackendResponse.Failure;
            }
            else
            {
                assets.Add(metadata.ID, new KeyValuePair<CableBeachMessages.MetadataBlock, byte[]>(metadata, assetData));
                return BackendResponse.Success;
            }
        }

        public BackendResponse TryFetchData(OpenMetaverse.UUID assetID, out byte[] assetData)
        {
            KeyValuePair<CableBeachMessages.MetadataBlock, byte[]> data;
            if (assets.TryGetValue(assetID, out data))
            {
                assetData = data.Value;
                return BackendResponse.Success;
            }
            assetData = null;
            return BackendResponse.NotFound;
        }

        public BackendResponse TryFetchDataMetadata(OpenMetaverse.UUID assetID, out CableBeachMessages.MetadataBlock metadata, out byte[] assetData)
        {
            throw new NotImplementedException();
        }

        public BackendResponse TryFetchMetadata(OpenMetaverse.UUID assetID, out CableBeachMessages.MetadataBlock metadata)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
