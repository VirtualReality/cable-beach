﻿using NUnit.Framework;
using InventoryServer.Extensions;
using HttpServer;
using HttpListener = HttpServer.HttpListener;
using System.Net;
using System.Collections.Generic;
using System;
using CableBeachMessages;
using OpenMetaverse;

namespace InventoryServer.Tests
{
    [TestFixture]
    public class WebDAVFrontendTest
    {
        InventoryServer inventoryServer = null;
        WebDAVFrontend webdav = null;
        string username = "http://127.0.0.1:8002/users/Test.User";
        string urlBase = "http://127.0.0.1:6969/inventory/";
        string password = String.Empty;
        UUID userID = new UUID("043d6d9f-d740-4ab2-98ab-11c908ff5c44");

        [SetUp]
        public void Init()
        {
            inventoryServer = new InventoryServer();
            string hostname = Dns.GetHostName();
            IPHostEntry entry = Dns.GetHostEntry(hostname);
            IPAddress address = IPAddress.Any;
            inventoryServer.FilesystemProvider = new DummyFileSystemProvider();
            inventoryServer.AssetProvider = new DummyAssetProvider();
            inventoryServer.WebDAVPropertyProvider = new DummyPropertyProvider();
            inventoryServer.HttpServer = HttpListener.Create(log4netLogWriter.Instance, address, 6969);
            inventoryServer.HttpServer.Start(10);
            webdav = new WebDAVFrontend();
            webdav.Start(inventoryServer);
            //webdav.AuthenticationType = WebDAVSharp.AuthenticationType.None;
        }

        [TearDown]
        public void End()
        {
            inventoryServer.AssetProvider = null;
            inventoryServer.WebDAVPropertyProvider = null;
            inventoryServer.FilesystemProvider = null;
            webdav.Stop();
            inventoryServer.HttpServer.Stop();
            inventoryServer = null;
            GC.Collect();
        }

        [Test]
        public void PropFindTest()
        {
            System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
            System.Uri uri = new System.Uri(urlBase);
            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("depth", "1");
            byte[] responseBody;
            HttpWebResponse response = MakeRequest(uri, "PROPFIND", headers, null, out responseBody);
            Assert.AreEqual(207, (int)response.StatusCode);
            string body = encoding.GetString(responseBody);
            Console.WriteLine(body);
            Assert.IsNotNull(body);
        }

        [Test]
        public void MkcolTest()
        {
            System.Uri uri = new System.Uri(urlBase+"test/");
            byte[] responseBody;
            HttpWebResponse response = MakeRequest(uri, "MKCOL", null, null, out responseBody);

            Assert.AreEqual(201, (int)response.StatusCode);
            
            //test that the new folder actually exists
            Uri identity = new Uri(username);
            OpenMetaverse.UUID agentID = inventoryServer.FilesystemProvider.IdentityToUUID(identity);
            if(!DoesFolderExistInInventory(identity, agentID, "test"))
                Assert.Fail("Could not find folder created with MKCOL");
        }

        [Test]
        public void MkcolSubFolder()
        {
            Uri identity = new Uri(username);
            OpenMetaverse.UUID agentID = inventoryServer.FilesystemProvider.IdentityToUUID(identity);
            InventoryFolder folder = new InventoryFolder("test", agentID, inventoryServer.FilesystemProvider.GetDefaultParent(identity, agentID, ""), 0);
            inventoryServer.FilesystemProvider.TryCreateFolder(identity, agentID, folder);
            byte[] responseBody;
            System.Uri uri2 = new System.Uri(urlBase + "test/fold/");
            HttpWebResponse response2 = MakeRequest(uri2, "MKCOL", null, null, out responseBody);
            Assert.AreEqual(201, (int)response2.StatusCode);

            //test that subfolder actually exists
            InventoryCollection inventory;
            if (inventoryServer.FilesystemProvider.TryFetchFilesystem(identity, agentID, out inventory) == BackendResponse.Success)
            {
                InventoryFolder testFolder = null;
                InventoryFolder foldFolder = null;
                foreach (var item in inventory.Folders.Values)
                {
                    if (item.Name == "test")
                        testFolder = item;
                    if (item.Name == "fold")
                        foldFolder = item;
                }

                if (foldFolder.ParentID != testFolder.ID)
                    Assert.Fail("Subfolder not created to right directory");
            }
            else
                Assert.Fail("Could not fetch inventory collection");
        }

        [Test]
        public void MkcolConflict()
        {
            System.Uri uri = new System.Uri(urlBase + "test/fold/");
            byte[] responseBody;
            HttpWebResponse response = MakeRequest(uri, "MKCOL", null, null, out responseBody);

            Assert.AreEqual(409, (int)response.StatusCode);
        }

        [Test]
        public void DeleteTest()
        {
            Uri identity = new Uri(username);
            OpenMetaverse.UUID agentID= inventoryServer.FilesystemProvider.IdentityToUUID(identity);
            InventoryFolder folder = new InventoryFolder("test", agentID, inventoryServer.FilesystemProvider.GetDefaultParent(identity, agentID, ""), 0);
            inventoryServer.FilesystemProvider.TryCreateFolder(identity, agentID, folder);
            OpenMetaverse.UUID folderID = folder.ID;

            System.Uri uri = new System.Uri(urlBase + "test/");
            byte[] responseBody;
            HttpWebResponse response = MakeRequest(uri, "DELETE", null, null, out responseBody);

            Assert.AreEqual(204, (int)response.StatusCode);

            //test that the folder is actually deleted
            InventoryFolder deletedFolder = null;
            if (BackendResponse.Success == inventoryServer.FilesystemProvider.TryFetchFolder(identity, agentID, folderID, out deletedFolder))
            {
                Assert.Fail("Found folder that should have been deleted");
            }
        }

        [Test]
        public void CopyTest()
        {
            Uri identity = new Uri(username);
            OpenMetaverse.UUID agentID = inventoryServer.FilesystemProvider.IdentityToUUID(identity);
            InventoryFolder folder = new InventoryFolder("test", agentID, inventoryServer.FilesystemProvider.GetDefaultParent(identity, agentID, ""), 0);
            inventoryServer.FilesystemProvider.TryCreateFolder(identity, agentID, folder);

            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("destination", urlBase + "test2");
            headers.Add("overwrite", "f");

            System.Uri uri = new System.Uri(urlBase + "test");
            byte[] responseBody;
            HttpWebResponse response = MakeRequest(uri, "COPY", headers, null, out responseBody);

            Assert.AreEqual(201, (int)response.StatusCode);

            //test that the copies actually exist
            if (!DoesFolderExistInInventory(identity, agentID, "test2"))
                Assert.Fail("Could not find folder created with COPY");
        }

        [Test]
        public void CopyTestOverwriteTrue()
        {
            Uri identity = new Uri(username);
            OpenMetaverse.UUID agentID = inventoryServer.FilesystemProvider.IdentityToUUID(identity);
            InventoryFolder folder = new InventoryFolder("test", agentID, inventoryServer.FilesystemProvider.GetDefaultParent(identity, agentID, ""), 0);
            inventoryServer.FilesystemProvider.TryCreateFolder(identity, agentID, folder);
            InventoryFolder folder2 = new InventoryFolder("test2", agentID, inventoryServer.FilesystemProvider.GetDefaultParent(identity, agentID, ""), 0);
            inventoryServer.FilesystemProvider.TryCreateFolder(identity, agentID, folder2);

            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("destination", urlBase + "test2");
            headers.Add("overwrite", "t");

            System.Uri uri = new System.Uri(urlBase + "test");
            byte[] responseBody;
            HttpWebResponse response = MakeRequest(uri, "COPY", headers, null, out responseBody);

            Assert.AreEqual(204, (int)response.StatusCode);

            //test that the copies actually exist
            if (!DoesFolderExistInInventory(identity, agentID, "test2"))
                Assert.Fail("Could not find folder created with COPY");
        }

        [Test]
        public void CopyTestOverwriteFalse()
        {
            Uri identity = new Uri(username);
            OpenMetaverse.UUID agentID = inventoryServer.FilesystemProvider.IdentityToUUID(identity);
            InventoryFolder folder = new InventoryFolder("test", agentID, inventoryServer.FilesystemProvider.GetDefaultParent(identity, agentID, ""), 0);
            inventoryServer.FilesystemProvider.TryCreateFolder(identity, agentID, folder);
            InventoryFolder folder2 = new InventoryFolder("test2", agentID, inventoryServer.FilesystemProvider.GetDefaultParent(identity, agentID, ""), 0);
            inventoryServer.FilesystemProvider.TryCreateFolder(identity, agentID, folder2);

            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("destination", urlBase + "test2");
            headers.Add("overwrite", "f");

            System.Uri uri = new System.Uri(urlBase + "test");
            byte[] responseBody;
            HttpWebResponse response = MakeRequest(uri, "COPY", headers, null, out responseBody);

            Assert.AreEqual(412, (int)response.StatusCode);
        }

        [Test]
        public void PutTest()
        {
            System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
            byte[] body = encoding.GetBytes("This is a simple test file.\n");

            System.Uri uri = new System.Uri(urlBase + "test.txt");
            byte[] responseBody;
            HttpWebResponse response = MakeRequest(uri, "PUT", null, body, out responseBody);

            Assert.AreEqual(201, (int)response.StatusCode);

            //that content is really in the inventory and assets
            Uri identity = new Uri(username);
            UUID agentID= inventoryServer.FilesystemProvider.IdentityToUUID(identity);
            InventoryFolder rootFolder;
            InventoryItem testItem = null;
            if (inventoryServer.FilesystemProvider.TryFetchRootFolder(identity, agentID, out rootFolder) == BackendResponse.Success)
            {
                foreach (var item in rootFolder.Children.Values)
                {
                    Console.WriteLine(item.Name);
                    if (item.Name == "test.txt" &&
                        item is InventoryItem)
                    {
                        testItem = (InventoryItem)item;
                        break;
                    }
                }
                byte[] assetData = null;
                if (testItem != null)
                {
                    if (inventoryServer.AssetProvider.TryFetchData(testItem.AssetID, out assetData) == BackendResponse.Success)
                    {
                        string assetString = System.Text.ASCIIEncoding.ASCII.GetString(assetData);
                        if (assetString == "This is a simple test file.\n")
                        {
                            return;
                        }
                        else
                        {
                            Assert.Fail("Asset data is different than expected", assetString);
                            return;
                        }
                    }
                }
            }
            Assert.Fail("Could not get inventory item or asset created by PUT");
        }

        [Test]
        public void MoveRenameTest()
        {
            Uri identity = new Uri(username);
            OpenMetaverse.UUID agentID = inventoryServer.FilesystemProvider.IdentityToUUID(identity);
            InventoryFolder folder = new InventoryFolder("test", agentID, inventoryServer.FilesystemProvider.GetDefaultParent(identity, agentID, ""), 0);
            inventoryServer.FilesystemProvider.TryCreateFolder(identity, agentID, folder);

            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("destination", urlBase + "test2");
            headers.Add("overwrite", "f");

            System.Uri uri = new System.Uri(urlBase + "test");
            byte[] responseBody;
            HttpWebResponse response = MakeRequest(uri, "MOVE", headers, null, out responseBody);

            Assert.AreEqual(201, (int)response.StatusCode);

            //test that the move has been actually made
            if (!DoesFolderExistInInventory(identity, agentID, "test2"))
                Assert.Fail("Could not find folder renamed with MOVE");
            if (DoesFolderExistInInventory(identity, agentID, "test"))
                Assert.Fail("Found orginal folder that should have been renamed");
        }

        [Test]
        public void MoveTest()
        {
            Uri identity = new Uri(username);
            OpenMetaverse.UUID agentID = inventoryServer.FilesystemProvider.IdentityToUUID(identity);
            InventoryFolder folder = new InventoryFolder("test", agentID, inventoryServer.FilesystemProvider.GetDefaultParent(identity, agentID, ""), 0);
            inventoryServer.FilesystemProvider.TryCreateFolder(identity, agentID, folder);
            InventoryFolder folder2 = new InventoryFolder("case", agentID, folder.ID, 0);
            inventoryServer.FilesystemProvider.TryCreateFolder(identity, agentID, folder2);

            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("destination", urlBase + "test2");
            headers.Add("overwrite", "t");

            System.Uri uri = new System.Uri(urlBase + "test");
            byte[] responseBody;
            HttpWebResponse response = MakeRequest(uri, "MOVE", headers, null, out responseBody);

            Assert.AreEqual(201, (int)response.StatusCode);

            //test that folder and subfolder were moved
            if (!DoesFolderExistInInventory(identity, agentID, "test2"))
                Assert.Fail("Could not find folder created with MOVE");
            if (DoesFolderExistInInventory(identity, agentID, "test"))
                Assert.Fail("Found orginal folder that should have been removed");

            InventoryCollection inventory;
            if (inventoryServer.FilesystemProvider.TryFetchFilesystem(identity, agentID, out inventory) == BackendResponse.Success)
            {
                InventoryFolder testFolder = null;
                InventoryFolder caseFolder = null;
                foreach (var item in inventory.Folders.Values)
                {
                    if (item.Name == "test2")
                        testFolder = item;
                    if (item.Name == "case")
                        caseFolder = item;
                }

                if (caseFolder.ParentID != testFolder.ID)
                    Assert.Fail("Subfolder doesn't belong to folder moved");
            }
            else
                Assert.Fail("Could not fetch inventory collection");
        }

        [Test]
        public void GetTest()
        {
            System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
            byte[] assetData = encoding.GetBytes("This is a simple test file.\n");

            if (!AddTestFile("test.txt", assetData))
            {
                Assert.Fail("Failed to create item for get test");
                return;
            }

            System.Uri uri = new System.Uri(urlBase + "test.txt");
            byte[] responseBody;
            HttpWebResponse response = MakeRequest(uri, "GET", null, null, out responseBody);

            Assert.AreEqual(200, (int)response.StatusCode);

            //check that the body contents matches the body in request
            string body = encoding.GetString(responseBody);
            Assert.AreEqual("This is a simple test file.\n", body);
        }

        [Test]
        public void GetTest404()
        {
            System.Uri uri = new System.Uri(urlBase + "test.txt");
            byte[] responseBody;
            HttpWebResponse response = MakeRequest(uri, "GET", null, null, out responseBody);

            Assert.AreEqual(404, (int)response.StatusCode);
        }

        [Test]
        public void PropPatchTest()
        {
            System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
            byte[] assetData = encoding.GetBytes("This is a simple test file.\n");

            if (!AddTestFile("test.txt", assetData))
            {
                Assert.Fail("Failed to create item for prop patch test");
                return;
            }

            byte[] requestBody = encoding.GetBytes(
                "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n" +
                "<propertyupdate xmlns='DAV:' xmlns:X=\"http://www.example.com\">\n" +
                "   <set><prop><X:publish>true</X:publish></prop></set>\n" +
                "</propertyupdate>");

            System.Uri uri = new System.Uri(urlBase + "test.txt");
            byte[] responseBody;
            HttpWebResponse response = MakeRequest(uri, "PROPPATCH", null, requestBody, out responseBody);

            Assert.AreEqual(207, (int)response.StatusCode);

            Console.WriteLine(encoding.GetString(responseBody));
            response.Close();
            bool foundNewProp = false;
            WebDAVSharp.IWebDAVResource resource = inventoryServer.WebDAVPropertyProvider.Load("/inventory/test.txt");
            foreach (WebDAVSharp.WebDAVProperty prop in resource.CustomProperties)
            {
                Console.WriteLine(prop.Namespace + ":" + prop.Name + ":" + prop.Value);
                if (prop.Namespace == "http://www.example.com" &&
                    prop.Name == "publish" &&
                    prop.Value == "true")
                    foundNewProp = true;
            }
            if (!foundNewProp)
                Assert.Fail("Newly set property not found");
        }

        [Test]
        public void LockTest()
        {
            System.Uri uri = new System.Uri(urlBase+"test");
            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("Timeout", "Infinite");

            string reqBody = 
                "<?xml version=\"1.0\" encoding=\"utf-8\" ?>" +
                "<D:lockinfo xmlns:D='DAV:'>" +
                "   <D:lockscope><D:exclusive/></D:lockscope>" +
                "   <D:locktype><D:write/></D:locktype>" +
                "   <D:owner xmlns:x=\"http://www.cablebeach.com/ns/\">" +
                "      <x:lock-user>http://127.0.0.1:8002/users/Test.User</x:lock-user>" +
                "      <x:created-by>InventoryServer Unit Test</x:created-by>" +
                "   </D:owner>" +
                "</D:lockinfo>";
            System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
            byte[] body = encoding.GetBytes(reqBody);

            byte[] responseBody;
            HttpWebResponse response = MakeRequest(uri, "LOCK", headers, body, out responseBody);
            Assert.AreEqual(200, (int)response.StatusCode);

            //TODO: parse response data and check if info is correct
        }

        [Test]
        public void LockLockTest()
        {
            System.Uri uri = new System.Uri(urlBase + "test");
            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("Timeout", "Infinite");

            string reqBody =
                "<?xml version=\"1.0\" encoding=\"utf-8\" ?>" +
                "<D:lockinfo xmlns:D='DAV:'>" +
                "   <D:lockscope><D:exclusive/></D:lockscope>" +
                "   <D:locktype><D:write/></D:locktype>" +
                "   <D:owner xmlns:x=\"http://www.cablebeach.com/ns/\">" +
                "      <x:lock-user>http://127.0.0.1:8002/users/Test.User</x:lock-user>" +
                "      <x:created-by>InventoryServer Unit Test</x:created-by>" +
                "   </D:owner>" +
                "</D:lockinfo>";
            System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
            byte[] body = encoding.GetBytes(reqBody);

            byte[] responseBody;
            HttpWebResponse response = MakeRequest(uri, "LOCK", headers, body, out responseBody);
            Assert.AreEqual(200, (int)response.StatusCode);

            //create second request
            HttpWebResponse response2 = MakeRequest(uri, "LOCK", headers, body, out responseBody);
            Assert.AreEqual(423, (int)response2.StatusCode);
        }

        [Test]
        public void LockUnLockTest()
        {
            System.Uri uri = new System.Uri(urlBase + "test");
            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("Timeout", "Infinite");

            string reqBody =
                "<?xml version=\"1.0\" encoding=\"utf-8\" ?>" +
                "<D:lockinfo xmlns:D='DAV:'>" +
                "   <D:lockscope><D:exclusive/></D:lockscope>" +
                "   <D:locktype><D:write/></D:locktype>" +
                "   <D:owner xmlns:x=\"http://www.cablebeach.com/ns/\">" +
                "      <x:lock-user>http://127.0.0.1:8002/users/Test.User</x:lock-user>" +
                "      <x:created-by>InventoryServer Unit Test</x:created-by>" +
                "   </D:owner>" +
                "</D:lockinfo>";
            System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
            byte[] body = encoding.GetBytes(reqBody);

            byte[] responseBody;
            HttpWebResponse response = MakeRequest(uri, "LOCK", headers, body, out responseBody);
            Assert.AreEqual(200, (int)response.StatusCode);

            string lockToken = response.Headers["lock-token"];
            Assert.IsNotNull(lockToken);

            headers = new Dictionary<string, string>();
            headers.Add("Lock-Token", lockToken);
            HttpWebResponse response2 = MakeRequest(uri, "UNLOCK", headers, null, out responseBody);
            Assert.AreEqual(204, (int)response2.StatusCode);
        }

        [Test]
        public void UnlockPrecoditionFailTest()
        {
            System.Uri uri = new System.Uri(urlBase + "test");
            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("Lock-Token", "opaquelocktoken:043d6d9f-d740-4ab2-98ab-11c908ff5c44"); //random lock token
            byte[] responseBody;
            HttpWebResponse response2 = MakeRequest(uri, "UNLOCK", headers, null, out responseBody);
            Assert.AreEqual(412, (int)response2.StatusCode);
        }

        #region Helpers

        private bool DoesFolderExistInInventory(Uri owner, OpenMetaverse.UUID agentID, string folderName)
        {
            InventoryCollection inventory;
            if (BackendResponse.Success == inventoryServer.FilesystemProvider.TryFetchFilesystem(owner, agentID, out inventory))
            {
                foreach (var item in inventory.Folders.Values)
                {
                    if (item.Name == folderName)
                        return true;
                }
            }
            return false;
        }

        private HttpWebResponse MakeRequest(Uri uri, string method, Dictionary<string, string> headers, byte[] body, out byte[] responseBody)
        {
            System.Net.ServicePointManager.Expect100Continue = false; //turn "Expect 100-continue" to off. This causes request with body to hang and fail rest of the tests.
            HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create(uri);
            req.Method = method;
            if (headers != null && headers.Count > 0)
            {
                foreach (KeyValuePair<string, string> header in headers)
                {
                    req.Headers.Add(header.Key, header.Value);
                }
            }

            if (body != null)
            {
                req.ContentLength = body.Length;
                System.IO.Stream bodyStream = req.GetRequestStream();
                bodyStream.Write(body, 0, body.Length);
                bodyStream.Close();
            }

            HttpWebResponse response = null;
            try
            {
                response = (HttpWebResponse)req.GetResponse();
            }
            catch (WebException we)
            {
                response = (HttpWebResponse)we.Response;

                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    //create new request with www-auth headers
                    string authHeader = response.Headers["www-authenticate"];
                    string digestHeader = null;
                    if (CreateDigestHeader(authHeader, uri.AbsolutePath, method, out digestHeader))
                    {
                        response.Close(); //close the old response now
                        HttpWebRequest req2 = (HttpWebRequest)HttpWebRequest.Create(uri);
                        req2.Method = method;
                        if (headers != null && headers.Count > 0)
                        {
                            foreach (KeyValuePair<string, string> header in headers)
                            {
                                req2.Headers.Add(header.Key, header.Value);
                            }
                        }
                        
                        req2.Headers.Add("Authorization", digestHeader);
                        if (body != null)
                        {
                            req2.ContentLength = body.Length;
                            System.IO.Stream bodyStream = req2.GetRequestStream();
                            bodyStream.Write(body, 0, body.Length);
                            bodyStream.Close();
                        }

                        try
                        {
                            response = (HttpWebResponse)req2.GetResponse();
                        }
                        catch (WebException webE)
                        {
                            response = (HttpWebResponse)webE.Response;
                        }
                    }
                }
            }

            System.IO.Stream responseBodyStream = response.GetResponseStream();
            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            byte[] buffer = new byte[1024];
            int bytes;
            while ((bytes = responseBodyStream.Read(buffer, 0, buffer.Length)) > 0)
            {
                ms.Write(buffer, 0, bytes);
            }
            responseBody = ms.ToArray();
            responseBodyStream.Close();
            response.Close();
            return response;
        }

        private bool CreateDigestHeader(string digest, string path, string method, out string authHeader)
        {
            authHeader = String.Empty;
            if (digest.ToLower().StartsWith("digest"))
            {
                try
                {
                    Dictionary<string, string> dict = new Dictionary<string, string>();
                    string[] parts = digest.Substring(7).Split(new char[] { ',' });
                    foreach (string part in parts)
                    {
                        string[] subParts = part.Split(new char[] { '=' }, 2);
                        string key = subParts[0].Trim(new char[] { ' ', '\"' });
                        string val = subParts[1].Trim(new char[] { ' ', '\"' });
                        dict.Add(key, val);
                    }

                    authHeader = "Digest username=\""+ username+"\",";
                    authHeader += " realm=\""+ dict["realm"] + "\",";
                    authHeader += " nonce=\"" + dict["nonce"] + "\",";
                    authHeader += " uri=\"" + path + "\",";
                    authHeader += " qop=\"auth\",";
                    authHeader += " nc=00000001,";
                    authHeader += " cnonce=\"0a4f113b\",";

                    string ha1 = MD5HashString(username + ":" + dict["realm"] + ":" + password);
                    Console.WriteLine("ha1: " + ha1);
                    string ha2 = MD5HashString(method + ":" + path);
                    Console.WriteLine("ha2: " + ha2);

                    string response = MD5HashString(ha1 + ":" +
                                    dict["nonce"] + ":" +
                                    "00000001" + ":" +
                                    "0a4f113b" + ":" +
                                    "auth" + ":" +
                                    ha2);
                    authHeader += " response=\"" + response + "\"";
                    Console.WriteLine(authHeader);
                    return true;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        private string MD5HashString(string Value)
        {
            System.Security.Cryptography.MD5CryptoServiceProvider x = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] data = System.Text.Encoding.ASCII.GetBytes(Value);
            data = x.ComputeHash(data);
            string ret = "";
            for (int i = 0; i < data.Length; i++)
                ret += String.Format("{0:x02}", data[i]); //data[i].ToString("x2").ToLower();
            return ret;
        }

        private bool AddTestFile(string fileName, byte[] assetData)
        {
            Uri identity = new Uri(username);
            UUID agentID = inventoryServer.FilesystemProvider.IdentityToUUID(identity);

            MetadataDefault assetMetadata = new MetadataDefault();
            assetMetadata.ContentType = "application/octet-stream";
            assetMetadata.CreationDate = DateTime.Now;
            assetMetadata.Description = "";
            assetMetadata.ID = UUID.Random();
            assetMetadata.Methods = new Dictionary<string, Uri>();
            assetMetadata.Name = fileName;
            assetMetadata.SHA256 = OpenMetaverse.Utils.SHA256(assetData);
            assetMetadata.Temporary = false;

            if (inventoryServer.AssetProvider.TryCreateAsset(assetMetadata, assetData) == BackendResponse.Success)
            {
                InventoryFolder parentFolder = null;
                if (inventoryServer.FilesystemProvider.TryFetchRootFolder(identity, agentID, out parentFolder) == BackendResponse.Success)
                {
                    InventoryItem inventoryItem = new InventoryItem();
                    inventoryItem.AssetID = assetMetadata.ID;
                    inventoryItem.AssetType = parentFolder.Type;
                    inventoryItem.CreationDate = 0;
                    inventoryItem.Creator = agentID;
                    inventoryItem.Owner = agentID;
                    inventoryItem.CurrentPermissions = 2147483647;
                    inventoryItem.NextPermissions = 2147483647;
                    inventoryItem.BasePermissions = 2147483647;
                    inventoryItem.EveryOnePermissions = 2147483647;
                    inventoryItem.GroupPermissions = 2147483647;
                    inventoryItem.InvType = (int)CableBeachMessages.InventoryType.Object;
                    inventoryItem.GroupOwned = false;
                    inventoryItem.Description = assetMetadata.Description;
                    inventoryItem.ID = UUID.Random();
                    inventoryItem.Name = assetMetadata.Name;
                    inventoryItem.ParentID = parentFolder.ID;
                    inventoryItem.SalePrice = 0;

                    if (inventoryServer.FilesystemProvider.TryCreateItem(identity, agentID, inventoryItem) == BackendResponse.Success)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        #endregion
    }
}
