/* 
 * Copyright (c) Intel Corporation
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * -- Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * -- Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * -- Neither the name of the Intel Corporation nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE INTEL OR ITS
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

using System;
using System.Collections.Specialized;
using System.Globalization;
using System.Net;
using System.Xml;
using System.Xml.Serialization;
using OpenMetaverse;
using OpenMetaverse.StructuredData;
using HttpServer;

namespace InventoryServer
{
    public static class Utils
    {
        public static UUID GetAuthToken(IHttpRequest request)
        {
            UUID authToken = UUID.Zero;

            string[] authHeader = request.Headers.GetValues("Authorization");
            if (authHeader != null && authHeader.Length == 1)
            {
                // Example header:
                // Authorization: OpenGrid 65fda0b5-4446-42f5-b828-aaf644293646
                string[] authHeaderParts = authHeader[0].Split(' ');
                if (authHeaderParts.Length == 2 && authHeaderParts[0] == "OpenGrid")
                    UUID.TryParse(authHeaderParts[1], out authToken);
            }
            
            if (authToken == UUID.Zero && request.Cookies != null)
            {
                // Check for an authToken cookie to make logins browser-compatible
                RequestCookie authCookie = request.Cookies["authToken"];
                if (authCookie != null)
                    UUID.TryParse(authCookie.Value, out authToken);
            }

            return authToken;
        }

        public static bool TryGetOpenSimUUID(Uri avatarUri, out UUID avatarID)
        {
            string[] parts = avatarUri.Segments;
            return UUID.TryParse(parts[parts.Length - 1], out avatarID);
        }

        public class InventoryItemSerializer : XmlSerializer
        {           
        }

        public class InventoryFolderSerializer : XmlSerializer
        {            
        }
        
    }
}
