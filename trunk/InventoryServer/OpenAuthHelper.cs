﻿/* 
 * Copyright (c) Intel Corporation
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * -- Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * -- Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * -- Neither the name of the Intel Corporation nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE INTEL OR ITS
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using DotNetOpenAuth;
using DotNetOpenAuth.Messaging;
using DotNetOpenAuth.OAuth;
using DotNetOpenAuth.OAuth.ChannelElements;
using DotNetOpenAuth.OAuth.Messages;
using HttpServer;

namespace InventoryServer
{
    /// <summary>
    /// Various states an OAuth token can be in
    /// </summary>
    public enum TokenAuthorizationState : int
    {
        /// <summary>
        /// An unauthorized request token
        /// </summary>
        UnauthorizedRequestToken = 0,
        /// <summary>
        /// An authorized request token
        /// </summary>
        AuthorizedRequestToken = 1,
        /// <summary>
        /// An authorized access token
        /// </summary>
        AccessToken = 2
    }

    public class OAuthToken
    {
        public string Secret;
        public TokenAuthorizationState State;

        public OAuthToken(string secret, TokenAuthorizationState state)
        {
            Secret = secret;
            State = state;
        }
    }

    public static class OpenAuthHelper
    {
        public static ServiceProviderDescription CreateServiceProviderDescription(Uri httpBaseUri)
        {
            ServiceProviderDescription desc = new ServiceProviderDescription();
            desc.RequestTokenEndpoint = new MessageReceivingEndpoint(new Uri(httpBaseUri, "oauth/get_request_token"), HttpDeliveryMethods.PostRequest);
            desc.UserAuthorizationEndpoint = new MessageReceivingEndpoint(new Uri(httpBaseUri, "oauth/authorize_token"), HttpDeliveryMethods.GetRequest);
            desc.AccessTokenEndpoint = new MessageReceivingEndpoint(new Uri(httpBaseUri, "oauth/get_access_token"), HttpDeliveryMethods.PostRequest);
            desc.TamperProtectionElements = new ITamperProtectionChannelBindingElement[] { new HmacSha1SigningBindingElement() };

            return desc;
        }

        public static void OpenAuthResponseToHttp(IHttpResponse httpResponse, OutgoingWebResponse openAuthResponse)
        {
            httpResponse.Status = openAuthResponse.Status;
            foreach (string key in openAuthResponse.Headers.Keys)
                httpResponse.AddHeader(key, openAuthResponse.Headers[key]);
            if (!String.IsNullOrEmpty(openAuthResponse.Body))
                httpResponse.AddToBody(openAuthResponse.Body);
        }

        public static HttpRequestInfo GetRequestInfo(IHttpRequest request)
        {
            // Combine HTTP headers and URL query values
            WebHeaderCollection headers = new WebHeaderCollection();
            try { headers.Add(request.Headers); }
            catch (Exception) { }
            try { NameValueCollection query = HttpUtility.ParseQueryString(request.Uri.Query); headers.Add(query); }
            catch (Exception) { }

            string rawUrl = request.Uri.AbsolutePath + request.Uri.Query + request.Uri.Fragment;
            return new HttpRequestInfo(request.Method, request.Uri, rawUrl, headers, request.Body);
        }

        public static string CreateQueryString(IDictionary<string, string> args)
        {
            if (args.Count == 0)
            {
                return String.Empty;
            }
            System.Text.StringBuilder sb = new System.Text.StringBuilder(args.Count * 10);

            foreach (KeyValuePair<string, string> arg in args)
            {
                sb.Append(HttpUtility.UrlEncode(arg.Key));
                sb.Append('=');
                sb.Append(HttpUtility.UrlEncode(arg.Value));
                sb.Append('&');
            }
            sb.Length--; // remove trailing &

            return sb.ToString();
        }
    }

    public class InMemoryTokenManager : IServiceProviderTokenManager
    {
        #region Classes

        private class TokenInfo : IServiceProviderRequestToken
        {
            internal TokenInfo()
            {
                this.CreatedOn = DateTime.Now;
            }

            public string ConsumerKey { get; set; }
            public DateTime CreatedOn { get; set; }
            public string Token { get; set; }
            public string VerificationCode { get; set; }
            public Uri Callback { get; set; }
            public Version ConsumerVersion { get; set; }
            internal string Secret { get; set; }
        }

        private class ConsumerInfo : IConsumerDescription
        {
            public string Key { get; set; }
            public string Secret { get; set; }
            public X509Certificate2 Certificate { get; set; }
            public Uri Callback { get; set; }
            public VerificationCodeFormat VerificationCodeFormat { get; set; }
            public int VerificationCodeLength { get; set; }
        }

        #endregion Classes

        private Dictionary<string, TokenInfo> tokens = new Dictionary<string, TokenInfo>();

        /// <summary>
        /// Request tokens that have been issued, and whether they have been authorized yet.
        /// </summary>
        private Dictionary<string, bool> requestTokens = new Dictionary<string, bool>();

        /// <summary>
        /// Access tokens that have been issued and have not yet expired.
        /// </summary>
        private Dictionary<string, string> accessTokens = new Dictionary<string, string>();

        #region ITokenManager Members

        public string GetTokenSecret(string token)
        {
            return this.tokens[token].Secret;
        }

        public void StoreNewRequestToken(UnauthorizedTokenRequest request, ITokenSecretContainingMessage response)
        {
            this.tokens.Add(response.Token, new TokenInfo { ConsumerKey = request.ConsumerKey, Token = response.Token, Secret = response.TokenSecret });
            this.requestTokens.Add(response.Token, false);
        }

        /// <summary>
        /// Checks whether a given request token has already been authorized
        /// by some user for use by the Consumer that requested it.
        /// </summary>
        /// <param name="requestToken">The Consumer's request token.</param>
        /// <returns>
        /// True if the request token has already been fully authorized by the user
        /// who owns the relevant protected resources.  False if the token has not yet
        /// been authorized, has expired or does not exist.
        /// </returns>
        public bool IsRequestTokenAuthorized(string requestToken)
        {
            return this.requestTokens[requestToken];
        }

        public void ExpireRequestTokenAndStoreNewAccessToken(string consumerKey, string requestToken, string accessToken, string accessTokenSecret)
        {
            this.requestTokens.Remove(requestToken);
            this.accessTokens.Add(accessToken, accessToken);
            this.tokens.Remove(requestToken);
            this.tokens.Add(accessToken, new TokenInfo { Token = accessToken, Secret = accessTokenSecret });
        }

        /// <summary>
        /// Classifies a token as a request token or an access token.
        /// </summary>
        /// <param name="token">The token to classify.</param>
        /// <returns>Request or Access token, or invalid if the token is not recognized.</returns>
        public TokenType GetTokenType(string token)
        {
            if (this.requestTokens.ContainsKey(token))
                return TokenType.RequestToken;
            else if (this.accessTokens.ContainsKey(token))
                return TokenType.AccessToken;
            else
                return TokenType.InvalidToken;
        }

        #endregion

        #region IServiceProviderTokenManager Members

        public IConsumerDescription GetConsumer(string consumerKey)
        {
            // We don't keep a list of authorized consumers, everyone is welcome
            ConsumerInfo description = new ConsumerInfo();
            description.Key = consumerKey;
            return description;
        }

        public IServiceProviderRequestToken GetRequestToken(string token)
        {
            try { return this.tokens[token]; }
            catch (Exception) { throw new KeyNotFoundException("Unrecognized token"); }
        }

        public IServiceProviderAccessToken GetAccessToken(string token)
        {
            try { return (IServiceProviderAccessToken)this.tokens[token]; }
            catch (Exception) { throw new KeyNotFoundException("Unrecognized token"); }
        }

        #endregion

        /// <summary>
        /// Marks an existing token as authorized.
        /// </summary>
        /// <param name="requestToken">The request token.</param>
        public void AuthorizeRequestToken(string requestToken)
        {
            if (requestToken == null)
                throw new ArgumentNullException("requestToken");

            this.requestTokens[requestToken] = true;
        }

        public void UpdateToken(IServiceProviderRequestToken token)
        {
            try
            {
                TokenInfo tokenInfo = this.tokens[token.Token];
                tokenInfo.Callback = token.Callback;
                tokenInfo.ConsumerKey = token.ConsumerKey;
                tokenInfo.ConsumerVersion = token.ConsumerVersion;
                tokenInfo.CreatedOn = token.CreatedOn;
                tokenInfo.VerificationCode = token.VerificationCode;
            }
            catch (Exception) { throw new KeyNotFoundException("Unrecognized token"); }
        }
    }
}
