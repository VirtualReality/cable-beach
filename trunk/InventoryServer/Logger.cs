﻿/* 
 * Copyright (c) Intel Corporation
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * -- Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * -- Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * -- Neither the name of the Intel Corporation nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE INTEL OR ITS
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

using System;
using HttpServer;
using log4net;
using log4net.Config;

[assembly: log4net.Config.XmlConfigurator(ConfigFileExtension = "log4net")]

namespace InventoryServer
{
    /// <summary>
    /// Holds a reference to an instance of the log4net logging engine
    /// </summary>
    public static class log4netInstance
    {
        public static ILog Log;

        static log4netInstance()
        {
            Log = LogManager.GetLogger(System.Reflection.Assembly.GetExecutingAssembly().FullName);

            // If error level reporting isn't enabled we assume no logger is configured and initialize a default
            // ConsoleAppender
            if (!Log.Logger.IsEnabledFor(log4net.Core.Level.Error))
            {
                log4net.Appender.ConsoleAppender appender = new log4net.Appender.ConsoleAppender();
                appender.Layout = new log4net.Layout.PatternLayout("%timestamp [%thread] %-5level - %message%newline");
                BasicConfigurator.Configure(appender);

                Log.Info("No log configuration found, defaulting to console logging");
            }
        }
    }

    /// <summary>
    /// Singleton logging class for the application
    /// </summary>
    public static class Logger
    {
        public static void Debug(object message) { log4netInstance.Log.Debug(message); }
        public static void Debug(object message, Exception exception) { log4netInstance.Log.Debug(message, exception); }
        public static void DebugFormat(string format, object arg0) { log4netInstance.Log.DebugFormat(format, arg0); }
        public static void DebugFormat(string format, params object[] args) { log4netInstance.Log.DebugFormat(format, args); }
        public static void DebugFormat(IFormatProvider provider, string format, params object[] args) { log4netInstance.Log.DebugFormat(provider, format, args); }
        public static void DebugFormat(string format, object arg0, object arg1) { log4netInstance.Log.DebugFormat(format, arg0, arg1); }
        public static void DebugFormat(string format, object arg0, object arg1, object arg2) { log4netInstance.Log.DebugFormat(format, arg0, arg1, arg2); }
        public static void Error(object message) { log4netInstance.Log.Error(message); }
        public static void Error(object message, Exception exception) { log4netInstance.Log.Error(message, exception); }
        public static void ErrorFormat(string format, object arg0) { log4netInstance.Log.ErrorFormat(format, arg0); }
        public static void ErrorFormat(string format, params object[] args) { log4netInstance.Log.ErrorFormat(format, args); }
        public static void ErrorFormat(IFormatProvider provider, string format, params object[] args) { log4netInstance.Log.ErrorFormat(provider, format, args); }
        public static void ErrorFormat(string format, object arg0, object arg1) { log4netInstance.Log.ErrorFormat(format, arg0, arg1); }
        public static void ErrorFormat(string format, object arg0, object arg1, object arg2) { log4netInstance.Log.ErrorFormat(format, arg0, arg1, arg2); }
        public static void Fatal(object message) { log4netInstance.Log.Fatal(message); }
        public static void Fatal(object message, Exception exception) { log4netInstance.Log.Fatal(message, exception); }
        public static void FatalFormat(string format, object arg0) { log4netInstance.Log.FatalFormat(format, arg0); }
        public static void FatalFormat(string format, params object[] args) { log4netInstance.Log.FatalFormat(format, args); }
        public static void FatalFormat(IFormatProvider provider, string format, params object[] args) { log4netInstance.Log.FatalFormat(provider, format, args); }
        public static void FatalFormat(string format, object arg0, object arg1) { log4netInstance.Log.FatalFormat(format, arg0, arg1); }
        public static void FatalFormat(string format, object arg0, object arg1, object arg2) { log4netInstance.Log.FatalFormat(format, arg0, arg1, arg2); }
        public static void Info(object message) { log4netInstance.Log.Info(message); }
        public static void Info(object message, Exception exception) { log4netInstance.Log.Info(message, exception); }
        public static void InfoFormat(string format, object arg0) { log4netInstance.Log.InfoFormat(format, arg0); }
        public static void InfoFormat(string format, params object[] args) { log4netInstance.Log.InfoFormat(format, args); }
        public static void InfoFormat(IFormatProvider provider, string format, params object[] args) { log4netInstance.Log.InfoFormat(provider, format, args); }
        public static void InfoFormat(string format, object arg0, object arg1) { log4netInstance.Log.InfoFormat(format, arg0, arg1); }
        public static void InfoFormat(string format, object arg0, object arg1, object arg2) { log4netInstance.Log.InfoFormat(format, arg0, arg1, arg2); }
        public static void Warn(object message) { log4netInstance.Log.Warn(message); }
        public static void Warn(object message, Exception exception) { log4netInstance.Log.Warn(message, exception); }
        public static void WarnFormat(string format, object arg0) { log4netInstance.Log.WarnFormat(format, arg0); }
        public static void WarnFormat(string format, params object[] args) { log4netInstance.Log.WarnFormat(format, args); }
        public static void WarnFormat(IFormatProvider provider, string format, params object[] args) { log4netInstance.Log.WarnFormat(provider, format, args); }
        public static void WarnFormat(string format, object arg0, object arg1) { log4netInstance.Log.WarnFormat(format, arg0, arg1); }
        public static void WarnFormat(string format, object arg0, object arg1, object arg2) { log4netInstance.Log.WarnFormat(format, arg0, arg1, arg2); }
    }

    /// <summary>
    /// Implementation of the logging class for HttpServer using log4net
    /// </summary>
    public class log4netLogWriter : ILogWriter
    {
        /// <summary>
        /// Singleton instance of this class
        /// </summary>
        public static log4netLogWriter Instance = new log4netLogWriter(log4netInstance.Log);

        ILog Log;

        log4netLogWriter(ILog log)
        {
            Log = log;
        }

        public void Write(object source, LogPrio prio, string message)
        {
            switch (prio)
            {
                case LogPrio.Trace:
                    return; // This logging is very noisy
                case LogPrio.Debug:
                    Log.DebugFormat("{0}: {1}", source, message);
                    break;
                case LogPrio.Info:
                    Log.InfoFormat("{0}: {1}", source, message);
                    break;
                case LogPrio.Warning:
                    Log.WarnFormat("{0}: {1}", source, message);
                    break;
                case LogPrio.Error:
                    Log.ErrorFormat("{0}: {1}", source, message);
                    break;
                case LogPrio.Fatal:
                    Log.FatalFormat("{0}: {1}", source, message);
                    break;
            }
        }
    }
}
