﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebDAVSharp;

namespace InventoryServer
{
    public interface IPropertyProvider
    {
        bool Save(IWebDAVResource resource);
        IWebDAVResource Load(string path);
    }
}
