﻿/* 
 * Copyright (c) Intel Corporation
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * -- Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * -- Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * -- Neither the name of the Intel Corporation nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE INTEL OR ITS
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.IO;
using System.Xml;
using ExtensionLoader;
using HttpServer;
using OpenMetaverse;

namespace InventoryServer.Extensions.OpenSim
{
    public class OpenSimFrontend : IExtension<InventoryServer>
    {
        InventoryServer server;

        public OpenSimFrontend()
        {
        }

        public bool Start(InventoryServer server)
        {
            this.server = server;

            server.HttpServer.AddHandler("POST", null, @"^/CreateInventory", CreateInventoryHandler);
            server.HttpServer.AddHandler("POST", null, @"^/RootFolders", RootFoldersHandler);

            return true;
        }

        public void Stop()
        {
        }

        void CreateInventoryHandler(IHttpClientContext context, IHttpRequest request, IHttpResponse response)
        {
            Logger.Info("CreateInventoryHandler()");
            bool success = false;

            UUID ownerID = DeserializeUUID(request.Body);

            if (ownerID != UUID.Zero)
            {
                // Create a new skeleton inventory for this identity if one does not already exist
                if (server.FilesystemProvider.GetDefaultParent(null, ownerID, "application/vnd.ll.folder") == UUID.Zero)
                {
                    if (LindenInventoryHelper.CreateDefaultInventory(server, null, ownerID))
                    {
                        Logger.Info("[OpenSimFrontend] Created a default (Linden Lab style) inventory for " + ownerID);
                        success = true;
                    }
                    else
                    {
                        Logger.Error("[OpenSimFrontend] Failed to create a default inventory for " + ownerID);
                    }
                }
            }

            byte[] retData = SerializeBool(success);
            response.ContentLength = retData.Length;
            response.Body.Write(retData, 0, retData.Length);
        }

        void RootFoldersHandler(IHttpClientContext context, IHttpRequest request, IHttpResponse response)
        {
            Logger.Info("RootFoldersHandler()");

            UUID ownerID = DeserializeUUID(request.Body);

            if (ownerID != UUID.Zero)
            {
                List<InventoryFolder> skeleton;
                BackendResponse storageResponse = server.FilesystemProvider.TryFetchFolderList(null, ownerID, out skeleton);

                if (storageResponse == BackendResponse.Success)
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        SerializeFolderList(ms, skeleton);
                        byte[] buffer = ms.ToArray();
                        response.ContentLength = ms.Length;
                        response.Body.Write(buffer, 0, (int)ms.Length);
                    }
                }
                else if (storageResponse == BackendResponse.NotFound)
                {
                    // Return an empty set of inventory so the requester knows that
                    // an inventory needs to be created for this agent
                    using (MemoryStream ms = new MemoryStream())
                    {
                        SerializeFolderList(ms, new List<InventoryFolder>(0));
                        byte[] buffer = ms.ToArray();
                        response.ContentLength = ms.Length;
                        response.Body.Write(buffer, 0, (int)ms.Length);
                    }
                }
                else
                {
                    response.Status = HttpStatusCode.InternalServerError;
                }
            }
            else
            {
                response.Status = HttpStatusCode.BadRequest;
            }
        }

        private static UUID DeserializeUUID(Stream stream)
        {
            UUID id = UUID.Zero;

            try
            {
                using (XmlReader reader = XmlReader.Create(stream))
                {
                    reader.MoveToContent();
                    UUID.TryParse(reader.ReadElementContentAsString("guid", String.Empty), out id);
                }
            }
            catch (Exception ex)
            {
                Logger.Warn("[OpenSimFrontend] Failed to parse POST data (expecting guid): " + ex.Message);
            }

            return id;
        }

        private static byte[] SerializeBool(bool value)
        {
            byte[] buffer;
            MemoryStream ms = new MemoryStream();

            using (XmlWriter writer = XmlWriter.Create(ms))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("boolean");
                writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
                writer.WriteAttributeString("xmlns", "xsd", null, "http://www.w3.org/2001/XMLSchema");
                writer.WriteString(value.ToString().ToLower());
                writer.WriteEndElement();
                writer.WriteEndDocument();
                writer.Flush();
            }

            ms.Seek(0, SeekOrigin.Begin);
            buffer = ms.GetBuffer();
            Array.Resize<byte>(ref buffer, (int)ms.Length);
            ms.Close();

            return buffer;
        }

        private static void SerializeFolderList(Stream stream, List<InventoryFolder> folders)
        {
            using (XmlWriter writer = XmlWriter.Create(stream))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("ArrayOfInventoryFolderBase");
                writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
                writer.WriteAttributeString("xmlns", "xsd", null, "http://www.w3.org/2001/XMLSchema");

                if (folders != null)
                {
                    foreach (InventoryFolder folder in folders)
                    {
                        writer.WriteStartElement("InventoryFolderBase");
                        writer.WriteElementString("Name", folder.Name);
                        WriteUUID(writer, "Owner", folder.Owner);
                        WriteUUID(writer, "ParentID", folder.ParentID);
                        WriteUUID(writer, "ID", folder.ID);
                        writer.WriteElementString("Type", XmlConvert.ToString(folder.Type));
                        writer.WriteElementString("Version", XmlConvert.ToString(folder.Version));
                        writer.WriteEndElement();
                    }
                }

                writer.WriteEndElement();
                writer.WriteEndDocument();

                writer.Flush();
            }

            stream.Flush();
        }

        private static void WriteUUID(XmlWriter writer, string name, UUID id)
        {
            writer.WriteStartElement(name);
            writer.WriteElementString("Guid", XmlConvert.ToString(id.Guid));
            writer.WriteEndElement();
        }

        private static void ReadUUID(XmlReader reader, string name, out UUID id)
        {
            reader.ReadStartElement(name);
            UUID.TryParse(reader.ReadElementContentAsString("Guid", String.Empty), out id);
            reader.ReadEndElement();
        }
    }
}
