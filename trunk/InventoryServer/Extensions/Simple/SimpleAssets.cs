/* 
 * Copyright (c) Intel Corporation
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * -- Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * -- Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * -- Neither the name of the Intel Corporation nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE INTEL OR ITS
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

using System;
using System.Collections.Generic;
using System.Net;
using System.IO;
using ExtensionLoader;
using OpenMetaverse;
using OpenMetaverse.StructuredData;
using CableBeachMessages;

namespace InventoryServer.Extensions
{
    public class SimpleAssets : IExtension<InventoryServer>, IAssetProvider
    {
        const string EXTENSION_NAME = "SimpleAssets"; // Used in metrics reporting
        const string DEFAULT_DATA_DIR = InventoryServer.DATA_DIR + "SimpleAssets";
        const string TEMP_DATA_DIR = InventoryServer.DATA_DIR + "SimpleAssetsTemp";

        InventoryServer server;
        Dictionary<UUID, MetadataBlock> metadataStorage;
        Dictionary<UUID, string> filenames;

        public SimpleAssets()
        {
        }

        #region Required Interfaces

        public bool Start(InventoryServer server)
        {
            this.server = server;
            metadataStorage = new Dictionary<UUID, MetadataBlock>();
            filenames = new Dictionary<UUID, string>();

            LoadFiles(DEFAULT_DATA_DIR, false);
            LoadFiles(TEMP_DATA_DIR, true);

            Logger.InfoFormat("[SimpleAssets] Initialized the asset store index with metadata for {0} assets", metadataStorage.Count);

            return true;
        }

        public void Stop()
        {
            WipeTemporary();
        }

        public BackendResponse TryFetchMetadata(UUID assetID, out MetadataBlock metadata)
        {
            metadata = null;
            BackendResponse ret;

            if (metadataStorage.TryGetValue(assetID, out metadata))
                ret = BackendResponse.Success;
            else
                ret = BackendResponse.NotFound;

            server.MetricsProvider.LogAssetMetadataFetch(EXTENSION_NAME, ret, assetID, DateTime.Now);
            return ret;
        }

        public BackendResponse TryFetchData(UUID assetID, out byte[] assetData)
        {
            assetData = null;
            string filename;
            BackendResponse ret;

            if (filenames.TryGetValue(assetID, out filename))
            {
                try
                {
                    assetData = File.ReadAllBytes(filename);
                    ret = BackendResponse.Success;
                }
                catch (Exception ex)
                {
                    Logger.ErrorFormat("[SimpleAssets] Failed reading data for asset {0} from {1}: {2}", assetID, filename, ex.Message);
                    ret = BackendResponse.Failure;
                }
            }
            else
            {
                ret = BackendResponse.NotFound;
            }

            server.MetricsProvider.LogAssetDataFetch(EXTENSION_NAME, ret, assetID, (assetData != null ? assetData.Length : 0), DateTime.Now);
            return ret;
        }

        public BackendResponse TryFetchDataMetadata(UUID assetID, out MetadataBlock metadata, out byte[] assetData)
        {
            metadata = null;
            assetData = null;
            string filename;
            BackendResponse ret;

            if (metadataStorage.TryGetValue(assetID, out metadata) &&
                    filenames.TryGetValue(assetID, out filename))
            {
                try
                {
                    assetData = File.ReadAllBytes(filename);
                    ret = BackendResponse.Success;
                }
                catch (Exception ex)
                {
                    Logger.ErrorFormat("[SimpleAssets] Failed reading data for asset {0} from {1}: {2}", assetID, filename, ex.Message);
                    ret = BackendResponse.Failure;
                }
            }
            else
            {
                ret = BackendResponse.NotFound;
            }

            server.MetricsProvider.LogAssetMetadataFetch(EXTENSION_NAME, ret, assetID, DateTime.Now);
            server.MetricsProvider.LogAssetDataFetch(EXTENSION_NAME, ret, assetID, (assetData != null ? assetData.Length : 0), DateTime.Now);
            return ret;
        }

        public BackendResponse TryCreateAsset(MetadataBlock metadata, byte[] assetData, out UUID assetID)
        {
            assetID = metadata.ID = UUID.Random();
            return TryCreateAsset(metadata, assetData);
        }

        public BackendResponse TryCreateAsset(MetadataBlock metadata, byte[] assetData)
        {
            BackendResponse ret;

            string path;
            string filename = String.Format("{0}.{1}", metadata.ID, CableBeachUtils.ContentTypeToExtension(metadata.ContentType));

            if (metadata.Temporary)
                path = Path.Combine(TEMP_DATA_DIR, filename);
            else
                path = Path.Combine(DEFAULT_DATA_DIR, filename);

            try
            {
                File.WriteAllBytes(path, assetData);
                lock (filenames) filenames[metadata.ID] = path;

                // Set the creation date to right now
                metadata.CreationDate = DateTime.Now;

                lock (metadataStorage)
                    metadataStorage[metadata.ID] = metadata;

                ret = BackendResponse.Success;
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("[SimpleAssets] Failed writing data for asset {0} to {1}: {2}", metadata.ID, filename, ex.Message);
                ret = BackendResponse.Failure;
            }

            server.MetricsProvider.LogAssetCreate(EXTENSION_NAME, ret, metadata.ID, assetData.Length, DateTime.Now);
            return ret;
        }

        public int ForEach(Action<MetadataBlock> action, int start, int count)
        {
            int position = 0;
            int rowCount = 0;

            lock (metadataStorage)
            {
                foreach (MetadataBlock metadata in metadataStorage.Values)
                {
                    if (position >= start && position < start + count)
                    {
                        action(metadata);
                        ++rowCount;
                    }
                    ++position;
                }
            }

            return rowCount;
        }

        #endregion Required Interfaces

        void WipeTemporary()
        {
            if (Directory.Exists(TEMP_DATA_DIR))
            {
                try { Directory.Delete(TEMP_DATA_DIR); }
                catch (Exception ex) { Logger.Error(ex.Message); }
            }
        }

        void LoadFiles(string folder, bool temporary)
        {
            // Try to create the directory if it doesn't already exist
            if (!Directory.Exists(folder))
            {
                try { Directory.CreateDirectory(folder); }
                catch (Exception ex)
                {
                    Logger.Warn(ex.Message);
                    return;
                }
            }

            lock (metadataStorage)
            {
                try
                {
                    string[] assets = Directory.GetFiles(folder);

                    for (int i = 0; i < assets.Length; i++)
                    {
                        string filename = assets[i];
                        byte[] data = File.ReadAllBytes(filename);

                        string contentType = CableBeachUtils.ExtensionToContentType(Path.GetExtension(filename).TrimStart('.'));

                        MetadataBlock metadata;

                        switch (contentType)
                        {
                            case "image/x-j2c":
                                MetadataJPEG2000 j2kMetadata = new MetadataJPEG2000();
                                // FIXME: Parse the JPEG2000 files for this data
                                j2kMetadata.Components = 0;
                                j2kMetadata.LayerEnds = new int[0];
                                metadata = j2kMetadata;
                                break;
                            default:
                                metadata = new MetadataDefault();
                                break;
                        }

                        metadata.CreationDate = File.GetCreationTime(filename);
                        metadata.Description = String.Empty;
                        metadata.ID = SimpleUtils.ParseUUIDFromFilename(filename);
                        metadata.Name = SimpleUtils.ParseNameFromFilename(filename);
                        metadata.SHA256 = OpenMetaverse.Utils.SHA256(data);
                        metadata.Temporary = false;
                        metadata.ContentType = CableBeachUtils.ExtensionToContentType(Path.GetExtension(filename).TrimStart('.'));

                        // Store the loaded data
                        metadataStorage[metadata.ID] = metadata;
                        filenames[metadata.ID] = filename;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Warn(ex.Message);
                }
            }
        }
    }
}
