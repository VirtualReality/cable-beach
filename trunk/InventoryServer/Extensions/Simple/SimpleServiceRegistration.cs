﻿/* 
 * Copyright (c) Intel Corporation
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * -- Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * -- Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * -- Neither the name of the Intel Corporation nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE INTEL OR ITS
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

using System;
using System.Collections.Generic;
using ExtensionLoader;

namespace InventoryServer.Extensions.Simple
{
    public class SimpleServiceRegistration : IExtension<InventoryServer>, IServiceRegistrationProvider
    {
        InventoryServer server;
        Dictionary<Uri, CreateCapabilitiesCallback> serviceCallbacks = new Dictionary<Uri, CreateCapabilitiesCallback>();

        public SimpleServiceRegistration()
        {
        }

        public bool Start(InventoryServer server)
        {
            this.server = server;

            return true;
        }

        public void Stop()
        {
        }

        public void RegisterService(Uri serviceIdentifier, CreateCapabilitiesCallback capabilitiesCallback)
        {
            lock (serviceCallbacks)
                serviceCallbacks[serviceIdentifier] = capabilitiesCallback;
        }

        public void CreateCapabilities(Uri identity, ref Dictionary<Uri, Uri> capabilities)
        {
            lock (serviceCallbacks)
            {
                foreach (KeyValuePair<Uri, CreateCapabilitiesCallback> entry in serviceCallbacks)
                {
                    try { entry.Value(identity, ref capabilities); }
                    catch (Exception ex)
                    {
                        Logger.Error("[SimpleServiceRegistration] Service " + entry.Key +
                            " threw an exception during capability creation: " + ex.Message, ex);
                    }
                }
            }
        }
    }
}
