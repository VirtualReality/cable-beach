﻿/* 
 * Copyright (c) Intel Corporation
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * -- Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * -- Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * -- Neither the name of the Intel Corporation nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE INTEL OR ITS
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

using System;
using System.Collections.Generic;
using System.Net;
using ExtensionLoader;
using HttpServer;
using OpenMetaverse;
using OpenMetaverse.StructuredData;
using CableBeachMessages;

namespace InventoryServer.Extensions.Simple
{
    public class RequestCapabilitiesFrontend : IExtension<InventoryServer>
    {
        InventoryServer server;

        public RequestCapabilitiesFrontend()
        {
        }

        public bool Start(InventoryServer server)
        {
            this.server = server;

            server.HttpServer.AddHandler("POST", null, @"^/request_capabilities$", RequestCapabilitiesHandler);

            return true;
        }

        public void Stop()
        {
        }

        void RequestCapabilitiesHandler(IHttpClientContext client, IHttpRequest request, IHttpResponse response)
        {
            Logger.Debug("[RequestCapabilitiesFrontend] Entering capabilities request");

            // FIXME: Implement client certificate checking

            OSDMap requestMap;
            if (ServiceHelper.TryGetOSD(request, out requestMap))
            {
                RequestCapabilitiesMessage message = new RequestCapabilitiesMessage();
                message.Deserialize(requestMap);

                // Create a dictionary of requested capabilities
                Dictionary<Uri, Uri> capabilities = new Dictionary<Uri, Uri>(message.Capabilities.Length);
                for (int i = 0; i < message.Capabilities.Length; i++)
                    capabilities[message.Capabilities[i]] = null;

                Logger.Info("[RequestCapabilitiesFrontend] Handling " + capabilities.Count + " capability requests");

                // Allow each registered service to attempt to fill in the capabilities request
                server.ServiceRegistrationProvider.CreateCapabilities(message.Identity, ref capabilities);

                RequestCapabilitiesReplyMessage reply = new RequestCapabilitiesReplyMessage();
                reply.Capabilities = new Dictionary<Uri, Uri>(capabilities.Count);
                foreach (KeyValuePair<Uri, Uri> entry in capabilities)
                {
                    if (entry.Value != null)
                        reply.Capabilities[entry.Key] = entry.Value;
                    else
                        Logger.Warn("[RequestCapabilitiesFrontend] Capability was not created for service identifier " + entry.Key);
                }

                Logger.Info("[RequestCapabilitiesFrontend] Returning " + reply.Capabilities.Count + " capabilities from a seed capability request");
                ServiceHelper.SendResponse(response, reply.Serialize());
            }
            else
            {
                response.Status = HttpStatusCode.BadRequest;
            }
        }
    }
}
