﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ExtensionLoader;
using WebDAVSharp;

namespace InventoryServer.Extensions.WebDAV.PropertyStorage.Null
{
    public class NullStorage : IExtension<InventoryServer>, IPropertyProvider
    {
        #region IPropertyProvider Members

        private Dictionary<string, IWebDAVResource> m_resources = new Dictionary<string, IWebDAVResource>();

        public bool Save(WebDAVSharp.IWebDAVResource resource)
        {
            if (!m_resources.ContainsKey(resource.Path))
                m_resources.Add(resource.Path, resource);
            return true;
        }

        public IWebDAVResource Load(string path)
        {
            if (m_resources.ContainsKey(path))
                return m_resources[path];
            else
                return null;
        }

        #endregion

        #region IExtension<InventoryServer> Members

        InventoryServer m_server = null;

        public bool Start(InventoryServer owner)
        {
            m_server = owner;
            return true;
        }

        public void Stop()
        {
        }

        #endregion
    }
}
