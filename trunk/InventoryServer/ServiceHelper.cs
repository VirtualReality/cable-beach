﻿/* 
 * Copyright (c) 2008 Intel Corporation
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * -- Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * -- Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * -- Neither the name of the Intel Corporation nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE INTEL OR ITS
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using HttpServer;
using OpenMetaverse.StructuredData;

namespace InventoryServer
{
    public static class ServiceHelper
    {
        public static bool TryGetOSD(IHttpRequest request, out OSDMap map)
        {
            try
            {
                map = OSDParser.DeserializeJson(request.Body) as OSDMap;
                if (map != null)
                    return true;
                else
                    Logger.Warn("[CableBeachFrontend] Unparseable request to capability");
            }
            catch (Exception ex)
            {
                Logger.Warn("[CableBeachFrontend] Invalid request to capability: " + ex.Message);
            }

            map = null;
            return false;
        }

        public static void SendResponse(IHttpResponse response, OSDMap map)
        {
            byte[] responseData = Encoding.UTF8.GetBytes(OSDParser.SerializeJsonString(map));
            SendResponse(response, responseData);
        }

        public static void SendResponse(IHttpResponse response, byte[] responseData)
        {
            response.ContentLength = responseData.Length;
            response.ContentType = "application/json";
            response.Encoding = Encoding.UTF8;
            response.Body.Write(responseData, 0, responseData.Length);
        }

        #region Base64Methods

        public static string base64DecodeFromNet(string data)
        {
            try
            {
                data = data.Replace('-', '+');
                data = data.Replace('_', '/');
                byte[] decbuff = Convert.FromBase64String(data);
                return Encoding.UTF8.GetString(decbuff);
            }
            catch (FormatException)
            {
                return String.Empty;
            }
        }

        public static string base64EncodeToNet(string str)
        {
            byte[] encbuff = System.Text.Encoding.UTF8.GetBytes(str);
            string data = Convert.ToBase64String(encbuff);
            data = data.Replace('/', '_');
            data = data.Replace('+', '-');
            return data;
        }

        #endregion
    }
}
