﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenMetaverse;
using CableBeachMessages;

using AssetType = OpenMetaverse.AssetType;
using InventoryType = OpenMetaverse.InventoryType;

namespace InventoryServer
{
    public static class LindenInventoryHelper
    {
        public static bool CreateDefaultInventory(InventoryServer server, Uri identity, UUID agentID)
        {
            bool success = true;

            UUID rootFolder = CreateRootFolder(server, "My Inventory", identity, agentID);
            if (rootFolder == UUID.Zero)
            {
                Logger.Warn("[LindenInventoryHelper] Failed to create root Inventory folder for " + identity);
                return false;
            }

            // Create a default inventory
            CreateFolder(server, rootFolder, "Animations", AssetType.Animation, identity, agentID);
            CreateFolder(server, rootFolder, "Body Parts", AssetType.Bodypart, identity, agentID);
            CreateFolder(server, rootFolder, "Calling Cards", AssetType.CallingCard, identity, agentID);
            UUID clothingFolder = CreateFolder(server, rootFolder, "Clothing", AssetType.Clothing, identity, agentID);
            CreateFolder(server, rootFolder, "Gestures", AssetType.Gesture, identity, agentID);
            CreateFolder(server, rootFolder, "Landmarks", AssetType.Landmark, identity, agentID);
            CreateFolder(server, rootFolder, "Lost and Found", AssetType.LostAndFoundFolder, identity, agentID);
            CreateFolder(server, rootFolder, "Notecards", AssetType.Notecard, identity, agentID);
            CreateFolder(server, rootFolder, "Objects", AssetType.Object, identity, agentID);
            CreateFolder(server, rootFolder, "Photo Album", AssetType.SnapshotFolder, identity, agentID);
            CreateFolder(server, rootFolder, "Scripts", AssetType.LSLText, identity, agentID);
            CreateFolder(server, rootFolder, "Sounds", AssetType.Sound, identity, agentID);
            CreateFolder(server, rootFolder, "Textures", AssetType.Texture, identity, agentID);
            CreateFolder(server, rootFolder, "Trash", AssetType.TrashFolder, identity, agentID);

            #region Appearance

            if (clothingFolder == UUID.Zero)
            {
                Logger.Warn("[LindenInventoryHelper] Failed to create Clothing folder for " + identity);
                return false;
            }

            UUID outfitFolder = outfitFolder = CreateFolder(server, clothingFolder, "Default Outfit", AssetType.Unknown, identity, agentID);
            if (clothingFolder == UUID.Zero)
            {
                Logger.Warn("[LindenInventoryHelper] Failed to create Default Outfit folder for " + identity);
                return false;
            }

            success |= (UUID.Zero != CreateItem(server, outfitFolder, "Default Hair", "Default Hair",
                new UUID("d342e6c0-b9d2-11dc-95ff-0800200c9a66"), AssetType.Bodypart, InventoryType.Wearable, identity, agentID));
            success |= (UUID.Zero != CreateItem(server, outfitFolder, "Default Shape", "Default Shape",
                new UUID("66c41e39-38f9-f75a-024e-585989bfab73"), AssetType.Bodypart, InventoryType.Wearable, identity, agentID));
            success |= (UUID.Zero != CreateItem(server, outfitFolder, "Default Skin", "Default Skin",
                new UUID("77c41e39-38f9-f75a-024e-585989bbabbb"), AssetType.Bodypart, InventoryType.Wearable, identity, agentID));
            success |= (UUID.Zero != CreateItem(server, outfitFolder, "Default Pants", "Default Pants",
                new UUID("00000000-38f9-1111-024e-222222111120"), AssetType.Clothing, InventoryType.Wearable, identity, agentID));
            success |= (UUID.Zero != CreateItem(server, outfitFolder, "Default Shirt", "Default Shirt",
                new UUID("00000000-38f9-1111-024e-222222111110"), AssetType.Clothing, InventoryType.Wearable, identity, agentID));

            #endregion Appearance

            return success;
        }

        public static UUID CreateRootFolder(InventoryServer server, string name, Uri identity, UUID agentID)
        {
            InventoryFolder rootFolder = new InventoryFolder();
            rootFolder.Children = new Dictionary<UUID,InventoryBase>();
            rootFolder.ID = UUID.Random();
            rootFolder.Name = name;
            rootFolder.Owner = agentID;
            rootFolder.ParentID = UUID.Zero;
            rootFolder.Type = (short)AssetType.Folder;
            rootFolder.Version = 1;

            if (server.FilesystemProvider.TryCreateFilesystem(identity, agentID, rootFolder) == BackendResponse.Success)
                return rootFolder.ID;
            else
                return UUID.Zero;
        }

        public static UUID CreateFolder(InventoryServer server, UUID parentID, string name, AssetType preferredType, Uri identity, UUID agentID)
        {
            InventoryFolder folder = new InventoryFolder();
            folder.Children = new Dictionary<UUID, InventoryBase>();
            folder.ID = UUID.Random();
            folder.Name = name;
            folder.Owner = agentID;
            folder.ParentID = parentID;
            folder.Type = (short)preferredType;
            folder.Version = 1;

            if (server.FilesystemProvider.TryCreateFolder(identity, agentID, folder) == BackendResponse.Success)
                return folder.ID;
            else
                return UUID.Zero;
        }

        public static UUID CreateItem(InventoryServer server, UUID parentID, string name, string description, UUID assetID,
            AssetType assetType, InventoryType invType, Uri identity, UUID agentID)
        {
            InventoryItem item = new InventoryItem();
            item.AssetID = assetID;
            item.AssetType = (int)assetType;
            item.BasePermissions = 0x7FFFFFFFu;
            item.CreationDate = (int)OpenMetaverse.Utils.DateTimeToUnixTime(DateTime.Now);
            item.CurrentPermissions = 0x7FFFFFFFu;
            item.Description = description;
            item.EveryOnePermissions = 0x7FFFFFFFu;
            item.Flags = 0;
            item.GroupID = UUID.Zero;
            item.GroupOwned = false;
            item.GroupPermissions = 0x7FFFFFFFu;
            item.ID = UUID.Random();
            item.InvType = (int)invType;
            item.Name = name;
            item.NextPermissions = 0x7FFFFFFFu;
            item.Owner = agentID;
            item.ParentID = parentID;
            item.SalePrice = 10;
            item.SaleType = (byte)SaleType.Not;

            if (server.FilesystemProvider.TryCreateItem(identity, agentID, item) == BackendResponse.Success)
                return item.ID;
            else
                return UUID.Zero;
        }
    }
}
