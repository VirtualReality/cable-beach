<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<meta http-equiv="Content-Language" content="en-us" />		
		<title>Verify Your Identity</title>
		<link rel="StyleSheet" href="/content/style.css" type="text/css">
	</head>
<body>
	<div id="wrap" class="clearfix">
	
	    <h3>Verify Your Identity</h3><br/>
	    
	    <strong>{$realm}</strong> is requesting verification of your identity<br/>
		<br/>
		
		<form method="post" action="{$form_post_url}">
			<label for="identifier">OpenID URL:</label>
			<input type="text" name="identifier" id="identifier" disabled="true" value="{$identifier}" style="background-color:#EEEEEE;" />
			
			<label for="first_name">First Name:</label>
			<input type="text" name="first_name" id="first_name" disabled="true" value="{$first_name}" style="background-color:#EEEEEE;" />
			
			<label for="last_name">Last Name:</label>
			<input type="text" name="last_name" id="last_name" disabled="true" value="{$last_name}" style="background-color:#EEEEEE;" />
			
			<label for="last_name">Password:</label>
			<input type="password" name="password" id="password" /><br/>
			
			{$form_hidden_fields}
			
			<a href="{$form_post_url}&cancel=true">Cancel and go back</a>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="submit" value="Continue">
		</form>
	
	</div>
</body>
</html>
