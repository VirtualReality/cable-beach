<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<meta http-equiv="Content-Language" content="en-us" />		
		<title>{$first_name} {$last_name}</title>
	    <link rel="stylesheet" href="/content/style.css" type="text/css" />
	    <link rel="openid2.provider openid.server" href="{$openid_server_url}" />
	    <link rel="describedby" href="{xrd_url}" type="application/xrd+xml" />
	</head>
<body>
	<div id="wrap" class="clearfix">
	
	<h3>{$first_name} {$last_name}</h3>
	<br/>
	
	OpenID identifier page for {$first_name} {$last_name}

	</div>
</body>
</html>
