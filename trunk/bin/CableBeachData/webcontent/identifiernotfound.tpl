<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<meta http-equiv="Content-Language" content="en-us" />		
		<title>Page not found</title>
	    <link rel="StyleSheet" href="/content/style.css" type="text/css">
	</head>
<body>
	<div id="wrap" class="clearfix">
	
	<h3>User not found</h3>
	<br/>
	
	Check the name and the URL format. OpenID identifiers use the following form:<br/>
	<pre>/users/FirstName.LastName</pre>
    
	</div>
</body>
</html>
