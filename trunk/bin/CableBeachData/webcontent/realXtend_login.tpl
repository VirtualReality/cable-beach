<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>realXtend Taiga login</title>
  <link rel="stylesheet" type="text/css" href="/content/realXtend.css">
</head>
<body>
	<div>
	<h1>realXtend Taiga Login</h1>
		<div>
			{if $has_error}
			<ul><li class="level1"><div><font color="red"><strong>Error: {$error}</strong></font></div><br/></li></ul>
			{/if}
			{if $has_message}
			<ul><li class="level1"><div class="li"><font><strong>{$message}</strong></font></div><br/></li></ul>
			{/if}
		</div>
		<div class="block">
			<h2>OpenID</h2>
			<form method="post" action="/login/">
				<table cellspacing="10">
					<tr>
					  <td>
					    Give your OpenID URL
					  </td>
					</tr>
					<tr>
					  <td>
						<input type="text" name="openid_identifier" style="width:250px;background-repeat:no-repeat;background-position:0 50%;background-image:url(/content/openid-16x16.png);padding-left:18px;border-style:solid;border-width:1px;border-color:lightgray;height:23px;text-align:bottom;" /> 
						<input type="image" src="/content/realXtend/opensim-login.png" value="Login" style="vertical-align:top;">
					  </td>
					</tr>
				</table>
			</form>
			<table cellspacing="10" width="350">
				<tr>
					<td colspan="2">
						You can also login with a Google or Yahoo Account, click the icons to start
					</td>
				</tr>
				<tr>
					<td>
						<form method="post" action="/login/"><input type="hidden" name="openid_identifier" value="https://www.google.com/accounts/o8/id"/><input type="image" src="/content/realXtend/google-button.png" value="Login" alt="Login"/>
						</form>
					</td>
					<td>
						<form method="post" action="/login/"><input type="hidden" name="openid_identifier" value="http://www.yahoo.com/"/><input type="image" src="/content/realXtend/yahoo-button.png" value="Login" alt="Login"/>
						</form>
					</td>
				</tr>
			</table>
			<br/>
			</p>
		</div>
		<br>
<!--
		<div class="block">
			<h2>Facebook</h2>
			<table cellspacing="10">
				<tr>
				  <td>
					<div id="facebook"></div>
				  </td>
				</tr>
			</table>
		</div>
-->
	</div>

<!-- Facebook Javascript -->
<!--
<script src="http://static.ak.connect.facebook.com/js/api_lib/v0.4/FeatureLoader.js.php" type="text/javascript"></script>
<script type="text/javascript">
    function onConnected(user_id)
    {
        FB.Facebook.apiClient.users_getInfo(user_id, 'last_name, first_name',
            function(result, exception)
            {
                var first_name = result[0]['first_name'];
                var last_name = result[0]['last_name'];
                
                document.getElementById('facebook').innerHTML = 
                    'You are logged in as <strong>' + first_name + ' ' + last_name + '</strong> through Facebook Connect. ' +
                    'Click Sign In to connect to the realXtend Taiga server. If this is not you or you wish to login with a different account, click logout.' +
                    '<br/><br/>' +
                    '<a href="/facebook/login/"><img src="/content/realXtend/opensim-login.png" border="0" alt="Login"/></a>&nbsp;&nbsp;' +
                    '<a href="#" onclick="FB.Connect.logout(function() { reload(); }); return false;">' +
                    '<img id="fb_logout_image" src="/content/facebook_logout_large.gif" alt="Logout" border="0"/>' +
                    '</a>';
            });
    }
    
    function onNotConnected()
    {
        document.getElementById('facebook').innerHTML = 
            '<a href="#" onclick="FB.Connect.requireSession(function() { reload(); }); return false;" >' +
            '<img id="fb_login_image" src="http://static.ak.fbcdn.net/images/fbconnect/login-buttons/connect_light_large_long.gif" alt="Connect" border="0"/>' +
            '</a>';
    }

    FB.init("***YOUR-FACEBOOK-API-KEY-HERE***", "/content/xd_receiver.htm", {"ifUserConnected":onConnected, "ifUserNotConnected":onNotConnected});

</script>
-->
<!-- End Facebook Javascript -->

</body>
</html>
