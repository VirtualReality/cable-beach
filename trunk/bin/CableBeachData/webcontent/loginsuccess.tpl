<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<meta http-equiv="Content-Language" content="en-us" />		
		<title>Login</title>
		<link rel="StyleSheet" href="/content/style.css" type="text/css">
	</head>
<body>
	<div id="wrap" class="clearfix">
	
		<h3>{$identity}</h3><br/>
		If you have the Cable Beach Launcher installed, use this link:<br/>
		<strong><a href="{$cb_login_uri}">Login</a></strong><br/>
	    <br/>
	    Otherwise, launch your viewer with: <pre>-loginuri {$login_uri}</pre>
	</div>
</body>
</html>
