<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" xmlns:fb="http://www.facebook.com/2008/fbml">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<meta http-equiv="Content-Language" content="en-us" />		
		<title>Facebook Login</title>
	    <link rel="StyleSheet" href="/content/style.css" type="text/css">
	</head>
<body>
	<div id="wrap" class="clearfix">
	
	<div id="facebook">&nbsp;</div>

	</div>

<!-- Facebook Javascript -->
<script src="http://static.ak.connect.facebook.com/js/api_lib/v0.4/FeatureLoader.js.php" type="text/javascript"></script>
<script type="text/javascript">
    function onConnected(user_id)
    {
        FB.Facebook.apiClient.users_getInfo(user_id, 'last_name, first_name',
            function(result, exception)
            {
                var first_name = result[0]['first_name'];
                var last_name = result[0]['last_name'];
                
                document.getElementById('facebook').innerHTML = 
                    'You are logged in as <strong>' + first_name + ' ' + last_name + '</strong> through Facebook Connect. ' +
                    'Click login to authenticate with this account. If this is not you or you wish to login with a different account, click logout.' +
                    '<br/><br/>' +
                    '<form method="GET" action="/facebook/login/"><input type="submit" value="Login"/></a>&nbsp;&nbsp;' +
                    '<a href="#" onclick="FB.Connect.logout(function() { reload(); }); return false;">' +
                    '<img id="fb_logout_image" src="/content/facebook_logout_large.gif" alt="Logout" border="0" align="top"/>' +
                    '</a>';
            });
    }
    
    function onNotConnected()
    {
        document.getElementById('facebook').innerHTML = 
            '<a href="#" onclick="FB.Connect.requireSession(function() { reload(); }); return false;" >' +
            '<img id="fb_login_image" src="http://static.ak.fbcdn.net/images/fbconnect/login-buttons/connect_light_large_long.gif" alt="Connect" border="0"/>' +
            '</a>';
    }
    
    FB.init("{$fb_api_key}", "xd_receiver.htm", {"ifUserConnected":onConnected, "ifUserNotConnected":onNotConnected});
</script>
<!-- End Facebook Javascript -->

</body>
</html>
