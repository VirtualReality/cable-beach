<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<meta http-equiv="Content-Language" content="en-us" />		
		<title>Sign Up</title>
		<link rel="StyleSheet" href="/content/style.css" type="text/css">
	</head>
<body>
	<div id="wrap" class="clearfix">
	
	<form action="/dosignup" method="post">

	<label for="user_name">Your name</label>
	<input id="user_name" name="user[name]" size="30" type="text" />
	<label for="user_email">Email</label>
	<input id="user_email" name="user[email]" size="30" type="text" />
	
	<div class="column">
		<label for="user_password">Password</label>
		<input id="user_password" name="user[password]" size="30" type="password" />
	</div>
	<div class="column">
		<label for="user_password_confirmation">Confirm your password</label>
		<input id="user_password_confirmation" name="user[password_confirmation]" size="30" type="password" />
	</div>

	<br/>
	<input id="tos" name="tos" type="checkbox" value="1" /> I have read and agree to the <a href="/tos" target="_blank">Terms of Service</a> and <a href="/privacy" target="_blank">Privacy Policy</a>.
	
	<hr />
	<input type="submit" value="Signup &#187;" class="primary" />
    
    </form>

	</div>
</body>
</html>
