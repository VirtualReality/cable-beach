<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<meta http-equiv="Content-Language" content="en-us" />		
		<title>Confirm Access</title>
		<link rel="StyleSheet" href="/content/style.css" type="text/css">
	</head>
<body>
	<div id="wrap" class="clearfix">
	
		<form method="post" action="/oauth/user_authorize">
			<h3>{$identity}</h3><br/>
			Please confirm that you want to allow <strong>{$consumer}</strong> access to the following:
			<br/>
			<ul>
			{foreach from=$capabilities item=capability}
			    <li>{$capability}</li>
			{/foreach}
			</ul>
			<br/>
			<input type="hidden" name="callback" value="{$callback}"/>
			<input type="hidden" name="request_token" value="{$request_token}"/>
			<div class="column">
				<input type="submit" name="confirm" value="Confirm" class="primary"/>
			</div>
			<div class="column">
				 <input type="submit" name="cancel" value="Cancel"/>
			</div>
		</form>
	
	</div>
</body>
</html>
