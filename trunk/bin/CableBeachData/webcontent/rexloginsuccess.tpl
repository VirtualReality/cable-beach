<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<meta http-equiv="Content-Language" content="en-us" />		
		<title>LoginSuccess</title>
		<link rel="StyleSheet" href="/content/style.css" type="text/css">
	</head>
<body>
	
	<div id="wrap" class="clearfix">
		<img src="/content/rexheader_small.png" align="center"><br><br>
		<p>RealXtend login succesfull for {$identity}</p>
		<script type="text/javascript">
		function ReturnSuccessValue()
		{
			return '{$information}';
		}
		</script>
	</div>
</body>
</html>
